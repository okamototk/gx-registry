import { CacheModule } from '@nestjs/cache-manager'
import { Module } from '@nestjs/common'
import { ConfigurationCredentialService } from './configuration-credential.service'
import { ConfigurationController } from './configuration.controller'

@Module({
  imports: [CacheModule.register()],
  controllers: [ConfigurationController],
  providers: [ConfigurationCredentialService]
})
export class ConfigurationModule {}
