import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Header, UseInterceptors } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiProduces } from '@nestjs/swagger'
import { createDidDocument } from '../utils/did.utils'
import { ConfigurationCredentialService } from './configuration-credential.service'

@Controller('')
@UseInterceptors(CacheInterceptor)
export class ConfigurationController {
  constructor(private configurationCredentialService: ConfigurationCredentialService) {}

  @Get('configurations/credentials')
  @ApiOperation({
    description: 'Returns a VerifiableCredentials exposing the current instance configuration'
  })
  @ApiOkResponse({})
  getConfigurationCredential() {
    return this.configurationCredentialService.getConfigurationCredential()
  }

  @Get(['.well-known/did.json', 'did.json'])
  getDid() {
    return createDidDocument()
  }

  @ApiProduces('application/x-pem-file')
  @Get(['.well-known/x509CertificateChain.pem', 'x509CertificateChain.pem'])
  @Header('Content-Type', 'application/x-pem-file')
  getX509Certificate() {
    return process.env.x509Certificate
  }
}
