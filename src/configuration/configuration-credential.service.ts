import { BadRequestException, Injectable, Logger, OnModuleInit } from '@nestjs/common'
import crypto, { createHash } from 'crypto'
import * as jose from 'jose'
import * as jsonld from 'jsonld'
import { version } from '../../package.json'
import { getDidWeb } from '../utils/did.utils'
import overrideDocumentLoader from '../utils/static-document-loader'

@Injectable()
export class ConfigurationCredentialService implements OnModuleInit {
  private readonly logger = new Logger(ConfigurationCredentialService.name)
  private configurationCredential

  async onModuleInit() {
    if (!process.env.privateKey) {
      this.logger.error('Unable to load privateKey from environment')
      throw new Error('Unable to load privateKey from environment')
    }
    if (!process.env.x509Certificate) {
      this.logger.error('Unable to load x509Certificate from environment')
      throw new Error('Unable to load x509Certificate from environment')
    }
    this.getConfigurationCredential()
  }

  getConfigurationCredential() {
    if (!this.configurationCredential) {
      this.configurationCredential = this.buildCredential()
    }
    return this.configurationCredential
  }

  async buildCredential() {
    const credential = {
      '@context': [
        'http://schema.org/',
        'https://www.w3.org/2018/credentials/v1',
        'https://w3id.org/security/suites/jws-2020/v1',
        'https://w3id.org/okn/o/sd#'
      ],
      type: ['VerifiableCredential', 'https://w3id.org/okn/o/sd#Software'],
      id: process.env.BASE_URL,
      issuanceDate: new Date().toISOString(),
      credentialSubject: {
        id: `${process.env.BASE_URL}#cs`,
        'https://w3id.org/okn/o/sd#hasVersion': {
          '@type': 'https://w3id.org/okn/o/sd#SoftwareVersion',
          'https://w3id.org/okn/o/sd#hasVersionId': `${version}`,
          'https://w3id.org/okn/o/sd#hasConfiguration': {
            '@type': 'https://w3id.org/okn/o/sd#SoftwareConfiguration',
            'https://w3id.org/okn/o/sd#hasParameter': [
              {
                '@type': 'https://w3id.org/okn/o/sd#Parameter',
                'schema:name': 'evsslonly',
                'https://w3id.org/okn/o/sd#hasFixedValue': `${process.env.evsslonly}`
              },
              {
                '@type': 'https://w3id.org/okn/o/sd#Parameter',
                'schema:name': 'revocationListURL',
                'https://w3id.org/okn/o/sd#hasFixedValue': process.env.revocationListURL || `${process.env.BASE_URL}/revoked-issuers.txt`
              },
              {
                '@type': 'https://w3id.org/okn/o/sd#Parameter',
                'schema:name': 'trustedIssuersURL',
                'https://w3id.org/okn/o/sd#hasFixedValue': process.env.trustedIssuersURL || `${process.env.BASE_URL}/trusted-gxdch.yaml`
              }
            ]
          }
        }
      },
      proof: {
        type: 'JsonWebSignature2020',
        created: new Date().toISOString(),
        proofPurpose: 'assertionMethod',
        verificationMethod: getDidWeb()
      }
    }
    const normalizedVC = await this.normalize(credential)
    const VCHash = createHash('sha256').update(normalizedVC).digest('hex')
    const jws = await this.sign(VCHash)
    credential.proof['jws'] = jws
    //sign cred
    return credential
  }

  async normalize(vc) {
    let canonized: string
    try {
      canonized = await jsonld.canonize(vc, {
        algorithm: 'URDNA2015',
        format: 'application/n-quads',
        documentLoader: overrideDocumentLoader
      })
    } catch (error) {
      console.log(error)
      throw new BadRequestException('Provided input is not a valid Self Description.', error.message)
    }
    if ('' === canonized) {
      throw new BadRequestException('Provided input is not a valid Self Description.', `Canonized SD is empty ${vc['id']}`)
    }

    return canonized
  }

  async sign(hash: string): Promise<string> {
    const alg = 'PS256'
    let jws
    if (process.env.privateKey.startsWith('-----BEGIN RSA PRIVATE KEY-----')) {
      const rsaPrivateKey = crypto.createPrivateKey(process.env.privateKey)
      jws = await new jose.CompactSign(new TextEncoder().encode(hash))
        .setProtectedHeader({
          alg,
          b64: false,
          crit: ['b64']
        })
        .sign(rsaPrivateKey)
    } else {
      const rsaPrivateKey = await jose.importPKCS8(process.env.privateKey, alg)
      jws = await new jose.CompactSign(new TextEncoder().encode(hash))
        .setProtectedHeader({
          alg,
          b64: false,
          crit: ['b64']
        })
        .sign(rsaPrivateKey)
    }

    return jws
  }
}
