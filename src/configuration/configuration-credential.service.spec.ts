import fs from 'fs'
import { JSONPath } from 'jsonpath-plus'
import { ConfigurationCredentialService } from './configuration-credential.service'

describe('configuration-credential.service', () => {
  beforeAll(() => {
    process.env.BASE_URL = 'https://asuperdomain.com'
    process.env.x509Certificate = fs.readFileSync('src/tests/cert.pem').toString()
    process.env.privateKey = fs.readFileSync('src/tests/key.pem').toString()
  })
  it('should return a configuration credential containing evsslonly, trustedIssuersURL and revocationListURL', async () => {
    //Given
    process.env.evsslonly = 'true'
    process.env.trustedIssuersURL = 'http://mydomain/trusted-issuers.yaml'
    process.env.revocationListURL = 'http://mydomain/revoked-issuers.yaml'
    //When
    const confCred = await new ConfigurationCredentialService().getConfigurationCredential()
    //Then
    expect(confCred).toBeDefined()
    expect(confCred.proof).toBeDefined()
    expect(confCred.credentialSubject).toBeDefined()
    expect(confCred.proof.jws).toBeDefined()
    const envNames = JSONPath({
      path: 'credentialSubject[https://w3id.org/okn/o/sd#hasVersion][https://w3id.org/okn/o/sd#hasConfiguration][https://w3id.org/okn/o/sd#hasParameter]..[schema:name]',
      json: confCred
    })
    const envValues = JSONPath({
      json: confCred,
      path: 'credentialSubject[https://w3id.org/okn/o/sd#hasVersion][https://w3id.org/okn/o/sd#hasConfiguration][https://w3id.org/okn/o/sd#hasParameter]..[https://w3id.org/okn/o/sd#hasFixedValue]'
    })
    expect(envNames).toEqual(['evsslonly', 'revocationListURL', 'trustedIssuersURL'])
    expect(envValues).toEqual(['true', 'http://mydomain/revoked-issuers.yaml', 'http://mydomain/trusted-issuers.yaml'])
  })
})
