import { Injectable, NotFoundException } from '@nestjs/common'
import fs from 'fs'

@Injectable()
export class ShapeService {
  private shapesBasePath = '/data/ipfs/registry'

  getTTLFile(version: string = 'development'): string {
    try {
      const filePath: string = `${this.shapesBasePath}/${version}/shapes/trustframework.ttl`
      fs.accessSync(filePath)

      return fs.readFileSync(filePath).toString()
    } catch {
      throw new NotFoundException(`Desired shape version '${version}' not found or not accessible`)
    }
  }
}
