import { NotFoundException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'
import fs from 'fs'
import { ShapeService } from './shape.service'

jest.mock('fs')

describe('ShapeService', () => {
  let service: ShapeService

  beforeEach(async () => {
    jest.resetAllMocks()

    const moduleRef = await Test.createTestingModule({
      providers: [ShapeService, ConfigService]
    }).compile()

    service = moduleRef.get<ShapeService>(ShapeService)
  })

  it('should collect a development shape by default', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockReturnValue('https://gaia-x.eu/ontology\nSome extra content for test purposes.')

    expect(service.getTTLFile()).toEqual('https://gaia-x.eu/ontology\nSome extra content for test purposes.')

    expect(fs.accessSync).toHaveBeenLastCalledWith('/data/ipfs/registry/development/shapes/trustframework.ttl')
    expect(fs.readFileSync).toHaveBeenLastCalledWith('/data/ipfs/registry/development/shapes/trustframework.ttl')
  })

  it('should collect a specific version of shape', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockReturnValue('https://gaia-x.eu/ontology\nSome extra content for test purposes.')

    expect(service.getTTLFile('2404')).toEqual('https://gaia-x.eu/ontology\nSome extra content for test purposes.')

    expect(fs.accessSync).toHaveBeenLastCalledWith('/data/ipfs/registry/2404/shapes/trustframework.ttl')
    expect(fs.readFileSync).toHaveBeenLastCalledWith('/data/ipfs/registry/2404/shapes/trustframework.ttl')
  })

  it('should throw an error when the shape does not exist', () => {
    jest.mocked(fs.accessSync).mockImplementation(() => {
      throw new Error('File is missing !')
    })

    expect(() => service.getTTLFile('1234')).toThrow(new NotFoundException(`Desired shape version '1234' not found or not accessible`))

    expect(fs.accessSync).toHaveBeenLastCalledWith('/data/ipfs/registry/1234/shapes/trustframework.ttl')
    expect(fs.readFileSync).not.toHaveBeenCalled()
  })
})
