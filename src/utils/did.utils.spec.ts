import fs from 'fs'
import { createDidDocument, getCertChainUri, getDidWeb } from './did.utils'

describe('did.utils', () => {
  it('should return a did composed of the component base url even containing several slashes', () => {
    //Given
    process.env.BASE_URL = 'https://domain.com/folder/subfolder'
    //When
    const didWeb = getDidWeb()
    //Then
    expect(didWeb).toEqual('did:web:domain.com:folder:subfolder')
  })
  it('should return a did composed of the component base url', () => {
    //Given
    process.env.BASE_URL = 'https://domain.com'
    //When
    const didWeb = getDidWeb()
    //Then
    expect(didWeb).toEqual('did:web:domain.com')
  })
  it('should return a certificate url using component base url', () => {
    //Given
    process.env.BASE_URL = 'https://mydomain.com'
    //When
    const certUrl = getCertChainUri()
    //Then
    expect(certUrl).toEqual('https://mydomain.com/.well-known/x509CertificateChain.pem')
  })
  it('should generate a did based on public key and component base url', async () => {
    //Given
    process.env.BASE_URL = 'https://asuperdomain.com'
    process.env.x509Certificate = fs.readFileSync('src/tests/cert.pem').toString()
    process.env.privateKey = fs.readFileSync('src/tests/key.pem').toString()
    //When
    const did = await createDidDocument()
    //Then
    expect(did).toBeDefined()
    expect(did.id).toEqual('did:web:asuperdomain.com')
    expect(did.verificationMethod[0].controller).toEqual('did:web:asuperdomain.com')
    expect(did.verificationMethod[0].id).toEqual('did:web:asuperdomain.com#X509-JWK2020')
    expect(did.verificationMethod[0].publicKeyJwk.alg).toEqual('RS256')
    expect(did.verificationMethod[0].publicKeyJwk.n).toEqual(
      'ttKIIfQi2JZ0pQw6hGbz-ANuMze6SJWENZTcsNlXecqsi4pV50dbUiosjxT0gtD3zDy_I1vDkoViT4-57QAc7pyB8cHMkq76U4uhY4gpwVe3vaVmV1BreXCtRpqCRrbeGYgKpYRazIvsKo5SJi4MqbIinUbu5shvoSk6A75gJUS32Jp1_VPj8SBv8NFZ2iwoKebAldMoqZWZeN-OGSnthCP_K6GEfZfWEVskhOtF6kqaIIDgrXFz0kulNNqoNA1EJoulKBunpfUP9hjnddnUl7TAOKTij5xxdXR32423LAWnwDwsiX57pSY3TlsXJ-Mjdm-8oUXzg3M4Kiz5G7q5Fw'
    )
    expect(did.verificationMethod[0].publicKeyJwk.kty).toEqual('RSA')
  })
})
