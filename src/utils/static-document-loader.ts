import fs from 'fs'
import * as jsonld from 'jsonld'
import { Url } from 'jsonld/jsonld-spec'

export default async (url: Url, options) => {
  const ctx1 = fs.readFileSync('src/utils/jsonld-contexts/credentials_v1_context.json', 'utf8')
  const ctx2 = fs.readFileSync('src/utils/jsonld-contexts/jws2020_v1_context.json', 'utf8')
  const ctx3 = fs.readFileSync('src/utils/jsonld-contexts/sd.json', 'utf8')
  const ctx4 = fs.readFileSync('src/utils/jsonld-contexts/trustframework_context.json', 'utf8')
  const CONTEXTS = {
    'https://www.w3.org/2018/credentials/v1': ctx1,
    'https://w3id.org/security/suites/jws-2020/v1': ctx2,
    'https://w3id.org/okn/o/sd#': ctx3,
    'https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#': ctx4
  }
  if (url in CONTEXTS) {
    return Promise.resolve({
      contextUrl: null, // this is for a context via a link header
      document: CONTEXTS[url], // this is the actual document that was loaded
      documentUrl: url // this is the actual context URL after redirects
    })
  } else {
    return jsonld['documentLoaders'].node()(url, options)
  }
}
