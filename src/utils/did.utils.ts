import * as jose from 'jose'
import { X509 } from 'jsrsasign'

//Src https://www.rfc-editor.org/rfc/rfc7518.html#appendix-A
const JCAMapping = {
  HmacSHA256: {
    JWS: 'HS256',
    OID: '1.2.840.113549.2.9'
  },
  HmacSHA384: {
    JWS: 'HS384',
    OID: '1.2.840.113549.2.10'
  },
  HmacSHA512: {
    JWS: 'HS512',
    OID: '1.2.840.113549.2.11'
  },
  SHA256withRSA: {
    JWS: 'RS256',
    OID: '1.2.840.113549.1.1.11'
  },
  SHA384withRSA: {
    JWS: 'RS384',
    OID: '1.2.840.113549.1.1.12'
  },
  SHA512withRSA: {
    JWS: 'RS512',
    OID: '1.2.840.113549.1.1.13'
  },
  SHA256withECDSA: {
    JWS: 'ES256',
    OID: '1.2.840.10045.4.3.2'
  },
  SHA384withECDSA: {
    JWS: 'ES384',
    OID: '1.2.840.10045.4.3.3'
  },
  SHA512withECDSA: {
    JWS: 'ES512',
    OID: '1.2.840.10045.4.3.4'
  },
  SHA256withRSAandMGF1: {
    JWS: 'PS256',
    OID: '1.2.840.113549.1.1.10'
  },
  SHA384withRSAandMGF1: {
    JWS: 'PS384',
    OID: '1.2.840.113549.1.1.10'
  },
  SHA512withRSAandMGF1: {
    JWS: 'PS512',
    OID: '1.2.840.113549.1.1.10'
  }
}
export const X509_VERIFICATION_METHOD_NAME = 'X509-JWK2020'

export function getDidWeb() {
  return `did:web:${process.env.BASE_URL.replace(/http[s]?:\/\//, '')
    .replace(':', '%3A') // encode port ':' as '%3A' in did:web
    .replace(/\//g, ':')}`
}

export function getCertChainUri() {
  return `${process.env.BASE_URL}/.well-known/x509CertificateChain.pem`
}

export async function createDidDocument() {
  const cert = new X509()
  cert.readCertPEM(process.env.x509Certificate)

  const spki = await jose.importX509(process.env.x509Certificate, JCAMapping[cert.getSignatureAlgorithmName()]?.JWS)
  const x509VerificationMethodIdentifier = `${getDidWeb()}#${X509_VERIFICATION_METHOD_NAME}`

  return {
    '@context': ['https://www.w3.org/ns/did/v1', 'https://w3id.org/security/suites/jws-2020/v1'],
    id: getDidWeb(),
    verificationMethod: [
      {
        '@context': 'https://w3c-ccg.github.io/lds-jws2020/contexts/v1/',
        id: x509VerificationMethodIdentifier,
        type: 'JsonWebKey2020',
        controller: getDidWeb(),
        publicKeyJwk: {
          ...(await jose.exportJWK(spki)),
          alg: JCAMapping[cert.getSignatureAlgorithmName()]?.JWS,
          x5u: getCertChainUri()
        }
      }
    ],
    assertionMethod: [x509VerificationMethodIdentifier]
  }
}
