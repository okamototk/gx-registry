import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose'
import { ScheduleModule } from '@nestjs/schedule'
import { ServeStaticModule } from '@nestjs/serve-static'
import { AppController } from './app.controller'
import { ConfigurationModule } from './configuration/configuration.module'
import { IpfsModule } from './ipfs/ipfs.module'
import { LinkMLOntologyModule } from './linkml-ontology/linkml-ontology.module'
import { OwlOntologyModule } from './owl-ontology/owl-ontology.module'
import { TrustedShapeRegistryModule } from './shape/trusted-shape-registry.module'
import { TrustAnchorModule } from './trust-anchor/trust-anchor.module'
import { TrustedAnchorModule } from './trusted-anchor/trusted-anchor.module'
import { TrustedIssuersModule } from './trusted-issuers/trusted-issuers.module'
import { TrustedSchemasRegistryModule } from './trusted-schemas-registry/trusted-schemas-registry.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      cache: true,
      isGlobal: true
    }),
    ScheduleModule.forRoot(),
    ServeStaticModule.forRoot({
      rootPath: process.env.IPFS_REGISTRY_PATH || '/data/ipfs/registry',
      serveRoot: process.env.APP_PATH,
      exclude: ['/api*', '/.well-known*']
    }),
    MongooseModule.forRoot(
      `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DATABASE}`,
      { connectTimeoutMS: 3_000, socketTimeoutMS: 20_000, waitQueueTimeoutMS: 1_000, serverSelectionTimeoutMS: 1_000 }
    ),
    ConfigurationModule,
    IpfsModule,
    TrustAnchorModule,
    TrustedIssuersModule,
    TrustedAnchorModule,
    TrustedShapeRegistryModule,
    TrustedSchemasRegistryModule,
    OwlOntologyModule,
    LinkMLOntologyModule
  ],
  controllers: [AppController]
})
export class AppModule {}
