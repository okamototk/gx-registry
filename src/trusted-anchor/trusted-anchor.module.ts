import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { TrustAnchorModule } from '../trust-anchor/trust-anchor.module'
import { TrustedListSerializerService } from './services/serializer/trusted-list-serializer.service'
import { XAdESSignatureService } from './services/signature/xades-signature.service'
import { TrustedAnchorController } from './trusted-anchor.controller'

@Module({
  imports: [HttpModule, TrustAnchorModule],
  controllers: [TrustedAnchorController],
  providers: [TrustedListSerializerService, XAdESSignatureService]
})
export class TrustedAnchorModule {}
