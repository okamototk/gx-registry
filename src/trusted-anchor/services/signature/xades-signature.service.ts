import { Injectable } from '@nestjs/common'
import { Crypto } from '@peculiar/webcrypto'
import forge from 'node-forge'
import { Application, Parse, SignedXml } from 'xadesjs'
import { stripPEMInfo } from '../../../common/util/stripPem'

/**
 * https://www.etsi.org/deliver/etsi_ts/101900_101999/101903/01.04.02_60/ts_101903v010402p.pdf
 * Service for XML Advanced Electronic Signatures (XAdES)
 */
@Injectable()
export class XAdESSignatureService {
  static {
    Application.setEngine('NodeJS', new Crypto())
  }

  /**
   * @param xmlString the payload to sign
   * @param x509Certificate the x509 certificate to extract public key from
   * @param algorithm the signing algorithm
   * @returns a XAdES BES signature
   * @see https://github.com/PeculiarVentures/xadesjs?tab=readme-ov-file#create-xades-bes-signature
   */
  async sign(
    xmlString: string,
    x509Certificate = process.env.xadesCertificate,
    algorithm: Algorithm = process.env.xadesPrivateKeyAlg
      ? JSON.parse(process.env.xadesPrivateKeyAlg)
      : { name: 'RSASSA-PKCS1-v1_5', hash: { name: 'SHA-256' } }
  ): Promise<string> {
    const privateKey = await Application.crypto.subtle.importKey(
      'pkcs8',
      Buffer.from(stripPEMInfo(process.env.xadesPrivateKey), 'base64'),
      algorithm,
      false,
      ['sign']
    )

    const certificate = forge.pki.certificateFromPem(x509Certificate)
    const publicKeyPem = stripPEMInfo(forge.pki.publicKeyToPem(certificate.publicKey))
    const publicKey = await Application.crypto.subtle.importKey('spki', Buffer.from(publicKeyPem, 'base64'), algorithm, true, ['verify'])

    const xmlDoc = Parse(xmlString)
    const signedXml = new SignedXml()
    const signature = await signedXml.Sign(algorithm, privateKey, xmlDoc, {
      keyValue: publicKey,
      references: [{ hash: 'SHA-256', transforms: ['enveloped', 'exc-c14n'] }],
      signingCertificate: stripPEMInfo(x509Certificate)
    })
    return signature.toString()
  }

  /**
   * @param xmlString the xml containing a XAdES BES signature to verify
   * @returns true if signature is valid
   * @throws error if digest verification fails
   * @see https://github.com/PeculiarVentures/xadesjs?tab=readme-ov-file#check-xades-bes-signature
   */
  async verify(xmlString: string): Promise<boolean> {
    const signedDocument = Parse(xmlString)
    const xmlSignature = signedDocument.getElementsByTagNameNS('http://www.w3.org/2000/09/xmldsig#', 'Signature')[0]
    const signedXml = new SignedXml(signedDocument)
    signedXml.LoadXml(xmlSignature)
    return signedXml.Verify()
  }
}
