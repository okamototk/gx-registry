import { Test, TestingModule } from '@nestjs/testing'
import fs from 'fs'
import { XAdESSignatureService } from './xades-signature.service'

// https://esignature.chorus-pro.gouv.fr/#/verifier/process
// https://ec.europa.eu/digital-building-blocks/DSS/webapp-demo/validation
// https://signatures-conformance-checker.etsi.org/
describe('XAdESSignatureService', () => {
  let xadesService: XAdESSignatureService

  beforeAll(async () => {
    process.env.xadesCertificate = fs.readFileSync('src/tests/xadesCert.pem').toString()
    process.env.xadesPrivateKey = fs.readFileSync('src/tests/xadesKey.pem').toString()

    const app: TestingModule = await Test.createTestingModule({
      providers: [XAdESSignatureService]
    }).compile()
    xadesService = app.get(XAdESSignatureService)
  })

  it('should create valid signature from XML string', async () => {
    const xmlString = '<root><data>Hello world</data></root>'
    const signature = await xadesService.sign(xmlString)
    const signed = xmlString.replace('</root>', `${signature}</root>`)
    expect(signed).toContain('Signature')

    const isOk = await xadesService.verify(signed)
    expect(isOk).toBeTruthy()
  })
})
