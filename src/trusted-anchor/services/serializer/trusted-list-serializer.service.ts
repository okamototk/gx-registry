import { Injectable } from '@nestjs/common'
import { create } from 'xmlbuilder2'
import { stripPEMInfo } from '../../../common/util/stripPem'
import { TrustAnchor } from '../../../trust-anchor/schemas'
import { XAdESSignatureService } from '../signature/xades-signature.service'
import { renderInformationTemplate } from './trusted-list-information-template'

const enLang = { 'xml:lang': 'en' }

/**
 * https://www.etsi.org/deliver/etsi_ts/119600_119699/119612/02.02.01_60/ts_119612v020201p.pdf
 * Serializer for ETSI TS 119 612 V2.2.1 (2016-04) format
 * Electronic Signatures and Infrastructures (ESI); Trusted Lists.
 * https://uri.etsi.org/19612/v2.2.1/ts_119612v020201_201601xsd.xsd
 */
@Injectable()
export class TrustedListSerializerService {
  constructor(private xadesSignatureService: XAdESSignatureService) {}

  public async serialize(anchors: TrustAnchor[]): Promise<string> {
    const schemeInformation = renderInformationTemplate({
      distributionUri: process.env.BASE_URL + '/api/trust-anchors',
      x509Certificate: stripPEMInfo(process.env.xadesCertificate)
    })
    const trustServiceStatusList = create({
      version: '1.0',
      encoding: 'UTF-8',
      standalone: false
    })
      .ele('TrustServiceStatusList', {
        xmlns: 'http://uri.etsi.org/02231/v2#',
        'xmlns:ns2': 'http://www.w3.org/2000/09/xmldsig#',
        'xmlns:ns3': 'http://uri.etsi.org/02231/v2/additionaltypes#',
        'xmlns:ns4': 'http://uri.etsi.org/01903/v1.3.2#',
        'xmlns:ns5': 'http://uri.etsi.org/TrstSvc/SvcInfoExt/eSigDir-1999-93-EC-TrustedList/#',
        'xmlns:ns6': 'http://uri.etsi.org/01903/v1.4.1#',
        TSLTag: 'http://uri.etsi.org/19612/TSLTag'
      })
      .ele(schemeInformation)
      .up()

    const list = trustServiceStatusList.ele('TrustServiceProviderList')
    for (const anchor of anchors) {
      const uri = anchor.uri ?? process.env.BASE_URL
      const provider = list.ele('TrustServiceProvider')

      const info = provider.ele('TSPInformation')
      info.ele('TSPName').ele('Name', enLang).txt(anchor.name)
      info.ele('TSPTradeName').ele('Name', enLang).txt(anchor.name)

      const address = info.ele('TSPAddress')
      const postalAddress = address.ele('PostalAddresses').ele('PostalAddress', enLang)
      postalAddress.ele('StreetAddress').txt('Unknown')
      postalAddress.ele('Locality').txt('Unknown')
      postalAddress.ele('CountryName').txt('Unknown')
      address.ele('ElectronicAddress').ele('URI', enLang).txt(uri)

      info.ele('TSPInformationURI').ele('URI', enLang).txt(uri)

      const serviceInfo = provider.ele('TSPServices').ele('TSPService').ele('ServiceInformation')
      serviceInfo.ele('ServiceTypeIdentifier').txt('http://uri.etsi.org/TrstSvc/Svctype/CA/QC')
      serviceInfo.ele('ServiceName').ele('Name', enLang).txt(anchor.name)
      serviceInfo
        .ele('ServiceDigitalIdentity')
        .ele('DigitalId')
        .ele('X509Certificate')
        .txt(stripPEMInfo(anchor.certificate ?? ''))
      serviceInfo.ele('ServiceStatus').txt('http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/granted')
      serviceInfo.ele('StatusStartingTime').txt(new Date(0).toISOString())
      serviceInfo
        .ele('TSPServiceDefinitionURI')
        .ele('URI', enLang)
        .txt(anchor._list?.uri ?? uri)

      const extensions = serviceInfo.ele('ServiceInformationExtensions')
      extensions
        .ele('Extension', { Critical: true })
        .ele('AdditionalServiceInformation')
        .ele('URI', enLang)
        .txt('http://uri.etsi.org/TrstSvc/TrustedList/SvcInfoExt/ForeSignatures')
    }
    const payloadToSign = trustServiceStatusList.toString()
    const signature = await this.xadesSignatureService.sign(payloadToSign)
    trustServiceStatusList.ele(signature)
    const headless = trustServiceStatusList.toString({
      headless: true,
      prettyPrint: false
    })
    return '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' + headless
  }
}
