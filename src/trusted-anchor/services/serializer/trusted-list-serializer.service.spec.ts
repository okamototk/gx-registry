import fs from 'fs'
import { validateXML } from 'xsd-schema-validator'
import { TrustAnchor } from '../../../trust-anchor/schemas'
import { TrustAnchorList } from '../../../trust-anchor/schemas/trust-anchor-list.schema'
import { XAdESSignatureService } from '../signature/xades-signature.service'
import { TrustedListSerializerService } from './trusted-list-serializer.service'

const fixturesDir = __dirname + '/../../../tests/fixtures/'

describe('TrustedListSerializerService', () => {
  const xAdESSignatureService = new XAdESSignatureService()
  const trustedListSerializerService = new TrustedListSerializerService(xAdESSignatureService)

  beforeAll(async () => {
    process.env.xadesCertificate = fs.readFileSync('src/tests/xadesCert.pem').toString()
    process.env.xadesPrivateKey = fs.readFileSync('src/tests/xadesKey.pem').toString()
    process.env.BASE_URL = 'https://gaia-x.eu'
  })

  async function verify(xmlString: string) {
    // verify XML content against ETSI TS 119 612 V2.2.1 trust anchor list schema with XSD
    const validationResult = await validateXML(xmlString, fixturesDir + 'ts_119612v020201_201601xsd.xsd')
    expect(validationResult.valid).toBe(true)

    // Verify XAdES signature
    const signatureValid = await xAdESSignatureService.verify(xmlString)
    expect(signatureValid).toBe(true)
  }

  it('should verify external ETSI TS 119 612 V2.2.1 trust anchor list correctly', async () => {
    const xmlString = fs.readFileSync(fixturesDir + 'valid-trust-anchor-list.xml', 'utf-8').toString()
    await verify(xmlString)
  })
  it('should create ETSI TS 119 612 V2.2.1 trust anchor list correctly', async () => {
    const anchors: TrustAnchor[] = [
      {
        certificate: process.env.x509Certificate,
        name: 'Test_Anchor',
        _list: new TrustAnchorList()
      }
    ]
    const xmlString = await trustedListSerializerService.serialize(anchors)
    expect(xmlString).toContain('Test_Anchor')
    await verify(xmlString)
  })
})
