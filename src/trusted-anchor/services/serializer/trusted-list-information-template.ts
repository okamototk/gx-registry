export function renderInformationTemplate(context: { distributionUri: string; x509Certificate: string }) {
  const now = new Date()
  const updateDelay = 600_000 // 10 minutes
  const sequenceNumber = Math.ceil(now.getTime() / updateDelay)
  const nextUpdate = new Date(now.getTime() + updateDelay)
  return `
<SchemeInformation>
  <TSLVersionIdentifier>5</TSLVersionIdentifier>
  <TSLSequenceNumber>${sequenceNumber}</TSLSequenceNumber>
  <TSLType>http://uri.etsi.org/TrstSvc/TrustedList/TSLType/EUgeneric</TSLType>
  <SchemeOperatorName>
    <Name xml:lang="en">Gaia-X European Association for Data and Cloud AISBL</Name>
  </SchemeOperatorName>
  <SchemeOperatorAddress>
    <PostalAddresses>
      <PostalAddress xml:lang="en">
        <StreetAddress>Avenue des Arts 6-9</StreetAddress>
        <Locality>Bruxelles</Locality>
        <PostalCode>1210</PostalCode>
        <CountryName>BE</CountryName>
      </PostalAddress>
    </PostalAddresses>
    <ElectronicAddress>
      <URI xml:lang="en">mailto:cto@gaia-x.eu</URI>
      <URI xml:lang="en">https://gaia-x.eu/</URI>
    </ElectronicAddress>
  </SchemeOperatorAddress>
  <SchemeName>
    <Name xml:lang="en">EU:Gaia-X Trusted list</Name>
  </SchemeName>
  <SchemeInformationURI>
    <URI xml:lang="en">https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/trust_anchors/</URI>
  </SchemeInformationURI>
  <StatusDeterminationApproach>http://uri.etsi.org/TrstSvc/TrustedList/StatusDetn/EUappropriate</StatusDeterminationApproach>
  <SchemeTypeCommunityRules>
    <URI xml:lang="en">http://uri.etsi.org/TrstSvc/TrustedList/schemerules/EUcommon</URI>
  </SchemeTypeCommunityRules>
  <SchemeTerritory>FR</SchemeTerritory>
  <PolicyOrLegalNotice>
    <TSLLegalNotice xml:lang="en">The present trusted list is provided by Gaia-X AISBL without warranty.</TSLLegalNotice>
  </PolicyOrLegalNotice>
  <HistoricalInformationPeriod>65535</HistoricalInformationPeriod>
  <PointersToOtherTSL>
    <OtherTSLPointer>
      <ServiceDigitalIdentities>
        <ServiceDigitalIdentity>
          <DigitalId>
            <X509Certificate>${context.x509Certificate}</X509Certificate>
          </DigitalId>
        </ServiceDigitalIdentity>
      </ServiceDigitalIdentities>
      <TSLLocation>${context.distributionUri}</TSLLocation>
      <AdditionalInformation>
        <OtherInformation>
          <ns3:MimeType>application/vnd.etsi.tsl+xml</ns3:MimeType>
        </OtherInformation>
        <OtherInformation>
          <SchemeOperatorName>
            <Name xml:lang="en">Gaia-X European Association for Data and Cloud AISBL</Name>
          </SchemeOperatorName>
        </OtherInformation>
      </AdditionalInformation>
    </OtherTSLPointer>
  </PointersToOtherTSL>
  <ListIssueDateTime>${now.toISOString()}</ListIssueDateTime>
  <NextUpdate>
    <dateTime>${nextUpdate.toISOString()}</dateTime>
  </NextUpdate>
  <DistributionPoints>
    <URI>${context.distributionUri}</URI>
  </DistributionPoints>
</SchemeInformation>`
}
