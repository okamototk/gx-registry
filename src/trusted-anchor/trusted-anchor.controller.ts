import { Controller, Get } from '@nestjs/common'
import { Header } from '@nestjs/common/decorators/http'
import { ApiOperation, ApiProduces, ApiTags } from '@nestjs/swagger'
import { TrustAnchorService } from '../trust-anchor/services/trust-anchor.service'
import { TrustedListSerializerService } from './services/serializer/trusted-list-serializer.service'

@ApiTags('TrustedAnchor')
@Controller({ path: '/api/trusted-anchors' })
export class TrustedAnchorController {
  constructor(
    private readonly trustAnchorService: TrustAnchorService,
    private readonly trustedListSerializerService: TrustedListSerializerService
  ) {}

  @Get()
  @Header('Content-Type', 'application/xml')
  @ApiProduces('application/xml')
  @ApiOperation({ summary: 'Get the list of all Trust Anchors in the registry (format ETSI TS 119 612)' })
  async getTrustAnchors() {
    return this.trustedListSerializerService.serialize(await this.trustAnchorService.getTrustAnchors())
  }
}
