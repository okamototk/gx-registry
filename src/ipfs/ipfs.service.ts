import { Injectable, Logger, OnModuleDestroy, OnModuleInit } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'
import * as dns from 'dns'
import { promises as fs } from 'fs'
import type { CID, IPFSHTTPClient } from 'kubo-rpc-client/dist/src/types'
import { dirname, join } from 'path'
import { promisify } from 'util'

const DOMAIN = 'gxdch.eu'
const resolveTxtAsync = promisify(dns.resolveTxt)

@Injectable()
export class IpfsService implements OnModuleInit, OnModuleDestroy {
  private readonly logger = new Logger(IpfsService.name)
  private registryRootCID: CID
  private ipfs: IPFSHTTPClient
  private CID

  constructor() {}

  async onModuleInit(): Promise<void> {
    const init = this.startNode()
    if (process.env.ASYNC_INIT !== 'true') {
      await init
    }
  }

  async onModuleDestroy(): Promise<void> {
    if (this.ipfs != null) {
      await this.ipfs.stop()
    }
  }

  private async startNode(): Promise<void> {
    this.logger.log('Connecting to local IPFS node...')

    const { create, CID } = await eval(`import('kubo-rpc-client')`)
    this.CID = CID
    // Initialize kubo-rpc-client
    this.ipfs = create({ host: process.env.KUBO_HOST, port: 5001 })

    this.logger.log('Connecting to local IPFS node...')
    try {
      const id = await this.ipfs.id()
      this.logger.log(`Connected to IPFS node, peer id: ${id.id}`)
      await this.fetchRegistryResources()
    } catch (error) {
      this.logger.error('Failed to connect to IPFS node:', error)
    }
  }

  @Cron(CronExpression.EVERY_10_MINUTES)
  private async fetchRegistryResources(): Promise<void> {
    const appBranch = process.env.APP_BRANCH || 'development'
    const records = await this.getTxtRecords(`${appBranch}.${DOMAIN}`)
    this.logger.log(`TXT records for ${appBranch}.${DOMAIN}: ${JSON.stringify(records)}`)
    const registryRecord = records.flat().find(str => str.startsWith('ipfs://'))

    if (registryRecord) {
      const cidValue = registryRecord.substring('ipfs://'.length)
      this.registryRootCID = this.CID.parse(cidValue)

      // Check if the CID points to a directory, timeout the request after 15 seconds
      const stat = await this.ipfs.files.stat('/ipfs/' + this.registryRootCID.toString(), { timeout: 15000 })
      if (stat.type !== 'directory') {
        throw new Error('Registry root CID is not a directory')
      }

      // Recursively list and save contents of the directory
      await this.listContentsAndSave(this.registryRootCID)
    } else {
      throw new Error(`Cannot load Registry root CID from DNS TXT record, 'ipfs://' prefix not found on ${appBranch}.${DOMAIN}`)
    }
  }
  private async getTxtRecords(domain: string): Promise<string[][]> {
    this.logger.log(`Getting TXT records for : ${domain}`)

    try {
      return await resolveTxtAsync(domain)
    } catch (err) {
      this.logger.log(`Couldn't get records for domain ${domain}, encountered error ${err.name} ${err.message}`)
      throw err
    }
  }

  private async listContentsAndSave(cid: CID, originalPath: string = ''): Promise<void> {
    for await (const file of this.ipfs.ls(cid)) {
      const fullPath = join(originalPath, file.name)
      if (file.type === 'dir') {
        await this.listContentsAndSave(file.cid, fullPath)
      } else {
        await this.saveFileFromIpfsToLocal(file.cid, fullPath)
      }
    }
  }

  public async saveFileFromIpfsToLocal(cid: CID, filepath: string): Promise<void> {
    const chunks: Buffer[] = []

    for await (const chunk of this.ipfs.cat(cid)) {
      chunks.push(Buffer.from(chunk))
    }

    // Combine all chunks into a single buffer
    const buffer = Buffer.concat(chunks)
    const finalPath = `/data/ipfs/registry/${filepath}`
    const tempPath = finalPath + '.tmp' // Temporary path to prevent interlocking

    // Ensure the directory exists
    const directory = dirname(finalPath)
    await fs.mkdir(directory, { recursive: true })

    // Write the temporary file
    await fs.writeFile(tempPath, buffer)

    // Rename to final path (atomic operation)
    await fs.rename(tempPath, finalPath)
    this.logger.log(`File updated at ${finalPath} with IPFS fetched CID ${cid.toString()}`)

    // Pin the file CID after saving it locally
    await this.ipfs.pin.add(cid)
    this.logger.log(`Pinned CID ${cid.toString()} in local IPFS node.`)

    // Become a peer for other clearing houses
    await this.provideFile(cid)
  }

  public async provideFile(cid: CID): Promise<void> {
    try {
      this.ipfs.dht.provide(cid)
      this.logger.log(`Provided CID ${cid.toString()} to the IPFS network.`)
    } catch (error) {
      this.logger.error(`Error providing CID ${cid.toString()}:`, error)
    }
  }
}
