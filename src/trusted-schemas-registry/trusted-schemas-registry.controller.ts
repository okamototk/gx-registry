import { Controller, Get, Param, Response } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { Response as Res } from 'express'
import { SchemaFilesApiResponse } from './decorator'
import { SchemaService } from './service'

@ApiTags('Trusted-Schema-registry')
@Controller({ path: '/schemas' })
export class TrustedSchemasRegistry {
  constructor(private readonly schemasService: SchemaService) {}

  @SchemaFilesApiResponse('Get the specified JSON-LD schema version')
  @Get('/:version?')
  getSchemaVersion(@Response({ passthrough: true }) res: Res, @Param('version') version?: string): string {
    try {
      res.set({ 'Content-Type': 'application/ld+json' })

      return this.schemasService.getSchemaFile(version)
    } catch (e) {
      console.log('Unable to get schema', e)
      throw e
    }
  }
}
