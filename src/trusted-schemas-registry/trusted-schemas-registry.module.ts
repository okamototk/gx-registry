import { Module } from '@nestjs/common'
import { SchemaService } from './service'
import { TrustedSchemasRegistry } from './trusted-schemas-registry.controller'

@Module({
  imports: [],
  controllers: [TrustedSchemasRegistry],
  providers: [SchemaService]
})
export class TrustedSchemasRegistryModule {}
