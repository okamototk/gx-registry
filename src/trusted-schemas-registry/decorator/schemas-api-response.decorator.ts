import { applyDecorators } from '@nestjs/common'
import { ApiBadRequestResponse, ApiOkResponse, ApiOperation, ApiParam } from '@nestjs/swagger'

export function SchemaFilesApiResponse(summary: string) {
  return applyDecorators(
    ApiOperation({ summary }),
    ApiParam({
      name: 'version',
      allowEmptyValue: true,
      required: false,
      description: 'Version of the desired schema (defaults to `development`)',
      examples: {
        empty: {
          value: '',
          description: 'Defaults to the latest development version'
        },
        development: {
          value: 'development',
          description: 'For the latest development version'
        },
        '2404': {
          value: '2404',
          description: 'For version 24.04 of the ontology'
        }
      }
    }),
    ApiBadRequestResponse({ description: 'Shape not found' }),
    ApiOkResponse({ description: 'Schemas as Jsonld' }) // TODO add me
  )
}
