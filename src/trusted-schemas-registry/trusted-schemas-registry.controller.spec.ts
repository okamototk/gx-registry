import { INestApplication } from '@nestjs/common'
import { Test, TestingModule } from '@nestjs/testing'
import request from 'supertest'
import { SchemaService } from './service'
import { TrustedSchemasRegistryModule } from './trusted-schemas-registry.module'

describe('TrustedSchemasRegistryController', () => {
  let app: INestApplication
  let schemaService: SchemaService

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [TrustedSchemasRegistryModule]
    }).compile()

    schemaService = moduleRef.get<SchemaService>(SchemaService)
    ;(schemaService as any).schemaBasePath = `${__dirname}/../tests/fixtures/schemas`

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  it(
    'should return the latest development schema',
    () =>
      request(app.getHttpServer())
        .get('/schemas')
        .expect(200)
        .expect('Content-Type', 'application/ld+json; charset=utf-8')
        .then(response => {
          expect(response.text.indexOf('"gx": "https://w3id.org/gaia-x/development#"')).toBeGreaterThan(-1)
        }),
    10000
  )

  it(
    'should return the 1234 version schema',
    () =>
      request(app.getHttpServer())
        .get('/schemas/1234')
        .expect(200)
        .expect('Content-Type', 'application/ld+json; charset=utf-8')
        .then(response => {
          expect(response.text.indexOf('"gx": "https://w3id.org/gaia-x/1234#"')).toBeGreaterThan(-1)
        }),
    10000
  )

  it(
    'should throw a 404 error when querying a non-existent version',
    () => request(app.getHttpServer()).get('/schemas/non-existent').expect(404).expect('Content-Type', 'application/json; charset=utf-8'),
    10000
  )
})
