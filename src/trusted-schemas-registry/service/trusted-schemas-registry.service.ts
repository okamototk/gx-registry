import { Injectable, Logger, NotFoundException } from '@nestjs/common'
import fs from 'fs'

@Injectable()
export class SchemasService2210 {
  private readonly logger = new Logger('Schema')

  async getJsonldContext(): Promise<any> {
    try {
      const uri: string = process.env.BASE_URL
      const context = {
        self: `${uri}/api/trusted-schemas-registry/v2/schemas/`,
        item: []
      }

      const jsonFiles = await this.getAvailableSchemasFiles()
      for (const file of jsonFiles) {
        context.item.push({
          schemaId: file,
          href: `${uri}/api/trusted-schemas-registry/v2/schemas/${file}`
        })
      }

      return context
    } catch (e) {
      console.log('error', e.message)
      throw e
    }
  }

  async getAvailableSchemasFiles(): Promise<Array<string>> {
    try {
      return fs.readdirSync('/data/ipfs/registry/schemas').map(filename => filename.split('.')[0])
    } catch (e) {
      throw new NotFoundException('Desired schema not found, version is incorrect or unimplemented')
    }
  }
}
