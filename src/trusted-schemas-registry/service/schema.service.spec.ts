import fs from 'fs'
import { SchemaService } from './schema.service'

jest.mock('fs')

describe('SchemaService', () => {
  const service: SchemaService = new SchemaService()

  beforeEach(() => {
    jest.resetAllMocks()
  })

  it('should provide the development schema file by default', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockReturnValue(Buffer.from('Test schema\nWith multiple lines'))

    expect(service.getSchemaFile()).toEqual('Test schema\nWith multiple lines')

    expect(fs.accessSync).toBeCalledWith('/data/ipfs/registry/development/schemas/trustframework.json')
    expect(fs.readFileSync).toBeCalledWith('/data/ipfs/registry/development/schemas/trustframework.json')
  })

  it('should provide a specific version of the schema file', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockReturnValue(Buffer.from('Test schema\nWith multiple lines'))

    expect(service.getSchemaFile('2404')).toEqual('Test schema\nWith multiple lines')

    expect(fs.accessSync).toBeCalledWith('/data/ipfs/registry/2404/schemas/trustframework.json')
    expect(fs.readFileSync).toBeCalledWith('/data/ipfs/registry/2404/schemas/trustframework.json')
  })

  it('should throw an exception when file is not accessible', () => {
    jest.mocked(fs.accessSync).mockImplementation(() => {
      throw new Error('Test Error')
    })

    expect(() => service.getSchemaFile()).toThrow("Desired schema version 'development' not found or not accessible")

    expect(fs.accessSync).toBeCalledWith('/data/ipfs/registry/development/schemas/trustframework.json')
    expect(fs.readFileSync).not.toHaveBeenCalled()
  })

  it('should throw an exception when file is not readable', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockImplementation(() => {
      throw new Error('Test Error')
    })

    expect(() => service.getSchemaFile('1234')).toThrow("Desired schema version '1234' not found or not accessible")

    expect(fs.accessSync).toBeCalledWith('/data/ipfs/registry/1234/schemas/trustframework.json')
    expect(fs.readFileSync).toBeCalledWith('/data/ipfs/registry/1234/schemas/trustframework.json')
  })
})
