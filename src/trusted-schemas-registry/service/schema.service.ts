import { Injectable, NotFoundException } from '@nestjs/common'
import fs from 'fs'

@Injectable()
export class SchemaService {
  private schemaBasePath = '/data/ipfs/registry'

  getSchemaFile(version: string = 'development'): string {
    try {
      const filePath: string = `${this.schemaBasePath}/${version}/schemas/trustframework.json`
      fs.accessSync(filePath)

      return fs.readFileSync(filePath).toString()
    } catch {
      throw new NotFoundException(`Desired schema version '${version}' not found or not accessible`)
    }
  }
}
