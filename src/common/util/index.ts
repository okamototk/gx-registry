import { Certificate } from 'pkijs'

export * from './swagger.util'

export function isEmpty(value: string | number | object | any[]): boolean {
  return (
    value == null ||
    (Array.isArray(value) && value.length === 0) ||
    (typeof value === 'object' && Object.keys(value).length === 0) ||
    (typeof value === 'string' && !value)
  )
}

export function isNotEmpty(value: string | number | object | any[]): boolean {
  return !isEmpty(value)
}

export function getValueAsArray<T>(value: T | T[]): T[] {
  return Array.isArray(value) ? value : [value]
}

export function getBufferFromBase64(str: string): ArrayBuffer {
  return Buffer.from(str, 'base64')
}

/**
 * Transforms base64 encoded strings into PKI.js Certificates
 * @param base64Certs the string array containing base64 encoded certificates
 * @returns {[Certificate]} the transformed certificates
 */
export function getCertificatesFromRaw(base64Certs: string[]): Certificate[] {
  const certs = []
  certs.push(...base64Certs.map(cert => Certificate.fromBER(getBufferFromBase64(cert))))

  return certs
}
