import { isNotEmpty } from '../util/index'

export function stripPEMInfo(PEMInfo: string): string {
  return PEMInfo.replace(/\r\n|\r|\n|\s/g, '').replace(/([']*-----(BEGIN|END)(CERTIFICATE|PKCS7|PRIVATEKEY|PUBLICKEY)-----[']*|\n)/gm, '')
}

export function splitPEM(pem: string): string[] {
  return pem
    .split(/-----BEGIN (?:CERTIFICATE|PKCS7)-----/)
    .map(stripPEMInfo)
    .filter(isNotEmpty)
}
