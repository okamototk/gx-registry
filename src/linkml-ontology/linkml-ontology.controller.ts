import { Controller, Get, Param, Response } from '@nestjs/common'
import { ApiBadRequestResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger'
import { Response as Res } from 'express'
import { LinkMLOntologyService } from './services/linkml-ontology.service'

@ApiTags('LinkML-Ontology')
@Controller({ path: '/linkml' })
export class LinkMLOntologyController {
  constructor(private readonly linkMLOntologyService: LinkMLOntologyService) {}

  @ApiOperation({ summary: 'Get a version of the Gaia-X LinkML ontology in the Yaml format' })
  @ApiBadRequestResponse({ description: 'LinkML ontology not found' })
  @ApiOkResponse({ description: 'LinkML ontology in the Yaml format' })
  @ApiParam({
    name: 'version',
    allowEmptyValue: false,
    required: true,
    description: 'the version of the LinkML ontology to request',
    examples: {
      development: {
        value: 'development',
        description: 'For the latest development version'
      },
      '2404': {
        value: '2404',
        description: 'For version 24.04 of the ontology'
      }
    }
  })
  @Get(':version/types.yaml')
  getLinkMLOntologyVersion(@Response({ passthrough: true }) res: Res, @Param('version') version: string): string {
    try {
      res.set({ 'Content-Type': 'application/yaml' })

      return this.linkMLOntologyService.getLinkMLOntology(version)
    } catch (e) {
      throw e
    }
  }

  @ApiOperation({ summary: 'Get the latest development version of the Gaia-X LinkML ontology in the Yaml format' })
  @ApiBadRequestResponse({ description: 'LinkML ontology not found' })
  @ApiOkResponse({ description: 'LinkML ontology in the Yaml format' })
  @Get('types.yaml')
  getLatestLinkMLOntology(@Response({ passthrough: true }) res: Res): string {
    try {
      res.set({ 'Content-Type': 'application/yaml' })

      return this.linkMLOntologyService.getLinkMLOntology()
    } catch (e) {
      throw e
    }
  }
}
