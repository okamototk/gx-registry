import { Injectable, NotFoundException } from '@nestjs/common'
import fs from 'fs'

@Injectable()
export class LinkMLOntologyService {
  private ontologyBasePath = '/data/ipfs/registry'

  getLinkMLOntology(version: string = 'development'): string {
    try {
      const filePath: string = `${this.ontologyBasePath}/${version}/linkml/linkml.yaml`
      fs.accessSync(filePath)

      return fs.readFileSync(filePath).toString()
    } catch {
      throw new NotFoundException(`Desired LinkML ontology version '${version}' not found or not accessible`)
    }
  }
}
