import { INestApplication } from '@nestjs/common'
import { Test, TestingModule } from '@nestjs/testing'
import request from 'supertest'
import { LinkMLOntologyModule } from './linkml-ontology.module'
import { LinkMLOntologyService } from './services/linkml-ontology.service'

describe('LinkMLOntologyController', () => {
  let app: INestApplication
  let linkMLOntologyService: LinkMLOntologyService

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [LinkMLOntologyModule]
    }).compile()

    linkMLOntologyService = moduleRef.get<LinkMLOntologyService>(LinkMLOntologyService)
    ;(linkMLOntologyService as any).ontologyBasePath = `${__dirname}/../tests/fixtures/linkml`

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  it(
    'should return the latest development LinkML ontology',
    () =>
      request(app.getHttpServer())
        .get('/linkml/types.yaml')
        .expect(200)
        .expect('Content-Type', 'application/yaml; charset=utf-8')
        .then(response => {
          expect(response.text.indexOf('https://w3id.org/gaia-x/development#')).toBeGreaterThan(-1)
          expect(response.text.indexOf('is_a: RegistrationNumber')).toBeGreaterThan(-1)
        }),
    10000
  )

  it(
    'should return the 1234 version LinkML ontology',
    () =>
      request(app.getHttpServer())
        .get('/linkml/1234/types.yaml')
        .expect(200)
        .expect('Content-Type', 'application/yaml; charset=utf-8')
        .then(response => {
          expect(response.text.indexOf('https://w3id.org/gaia-x/1234#')).toBeGreaterThan(-1)
          expect(response.text.indexOf('is_a: RegistrationNumber')).toBeGreaterThan(-1)
        }),
    10000
  )

  it(
    'should throw a 404 error when querying a non-existent version',
    () => request(app.getHttpServer()).get('/linkml/non-existent/types.yaml').expect(404).expect('Content-Type', 'application/json; charset=utf-8'),
    10000
  )
})
