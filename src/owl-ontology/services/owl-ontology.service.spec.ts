import { NotFoundException } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import fs from 'fs'
import { OwlOntologyService } from './owl-ontology.service'

jest.mock('fs')

describe('OwlOntologyService', () => {
  let service: OwlOntologyService

  beforeEach(async () => {
    jest.resetAllMocks()

    const moduleRef = await Test.createTestingModule({
      providers: [OwlOntologyService]
    }).compile()

    service = moduleRef.get<OwlOntologyService>(OwlOntologyService)
  })

  it('should collect a development OWL ontology by default', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockReturnValue('https://gaia-x.eu/ontology\nSome extra content for test purposes.')

    expect(service.getOwlFile()).toEqual('https://gaia-x.eu/ontology\nSome extra content for test purposes.')

    expect(fs.accessSync).toHaveBeenLastCalledWith('/data/ipfs/registry/development/owl/trustframework.ttl')
    expect(fs.readFileSync).toHaveBeenLastCalledWith('/data/ipfs/registry/development/owl/trustframework.ttl')
  })

  it('should collect a specific version of the OWL ontology', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockReturnValue('https://gaia-x.eu/ontology\nSome extra content for test purposes.')

    expect(service.getOwlFile('2404')).toEqual('https://gaia-x.eu/ontology\nSome extra content for test purposes.')

    expect(fs.accessSync).toHaveBeenLastCalledWith('/data/ipfs/registry/2404/owl/trustframework.ttl')
    expect(fs.readFileSync).toHaveBeenLastCalledWith('/data/ipfs/registry/2404/owl/trustframework.ttl')
  })

  it('should throw an error when the OWL ontology does not exist', () => {
    jest.mocked(fs.accessSync).mockImplementation(() => {
      throw new Error('File is missing !')
    })

    expect(() => service.getOwlFile('1234')).toThrow(new NotFoundException(`Desired OWL ontology version '1234' not found or not accessible`))

    expect(fs.accessSync).toHaveBeenLastCalledWith('/data/ipfs/registry/1234/owl/trustframework.ttl')
    expect(fs.readFileSync).not.toHaveBeenCalled()
  })
})
