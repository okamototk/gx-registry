import { Controller, Get, Param } from '@nestjs/common'
import { ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger'
import { TrustedIssuers, TrustedIssuersService } from './trusted-issuers.service'

@ApiTags('Trusted Issuers')
@Controller('/api/trusted-issuers')
export class TrustedIssuersController {
  constructor(private readonly trustedIssuersService: TrustedIssuersService) {}

  @Get('')
  @ApiOperation({ summary: 'Map of authorized issuers URL for each type of service' })
  async listTrustedIssuers(): Promise<TrustedIssuers> {
    return this.trustedIssuersService.getTrustedIssuers()
  }
  @Get('/:serviceType')
  @ApiOperation({ summary: 'List of authorized issuers URL for designated type of service' })
  @ApiParam({
    name: 'serviceType',
    required: true,
    allowEmptyValue: false,
    enum: ['compliance', 'registry', 'registration-notary'],
    description: 'Type of service to retrieve authorized issuers'
  })
  async listTrustedIssuersForAType(@Param('serviceType') serviceType: string): Promise<string[]> {
    return this.trustedIssuersService.getTrustedIssuersForAServiceType(serviceType)
  }
}
