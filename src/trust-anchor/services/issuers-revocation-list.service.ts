import { HttpService } from '@nestjs/axios'
import { Injectable, Logger } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'
import { promises as fs } from 'fs'
import { X509 } from 'jsrsasign'
import { firstValueFrom } from 'rxjs'

@Injectable()
export class IssuersRevocationListService {
  private readonly revocationListPath = '/data/ipfs/registry/revoked-issuers.txt'
  private readonly revocationListURL = process.env.revocationListURL
  private readonly logger = new Logger(IssuersRevocationListService.name)
  private isRevocationURLOverridden = false
  private _revokedIssuers: Map<string, X509> = new Map()

  constructor(private readonly httpService: HttpService) {
    if (process.env.revocationListURL) {
      try {
        new URL(this.revocationListURL)
        this.isRevocationURLOverridden = true
      } catch (error) {
        this.logger.error('Invalid format for overridden trusted revocation list URL, reverting back to default:', error)
      }
    }
  }

  async onModuleInit() {
    await this.loadRevokedIssuers()
    this.logger.log(`Initialized revocation list with ${this._revokedIssuers.size} entries`)
  }

  @Cron(CronExpression.EVERY_10_MINUTES)
  async loadRevokedIssuers() {
    this.logger.log('Start revocation list retrieval')
    let revokedIssuersData: string

    try {
      if (this.isRevocationURLOverridden) {
        try {
          revokedIssuersData = await this.getRevokedIssuersFromUrl()
        } catch (Error) {
          this.logger.error('Unable to retrieve revoked issuers From URL. Will try loading the standard one', Error)
          revokedIssuersData = await this.getRevokedIssuersFromFile()
        }
      } else {
        revokedIssuersData = await this.getRevokedIssuersFromFile()
      }
    } catch (Error) {
      this.logger.error('Unable to retrieve revoked issuers list. Wont replace existing one', Error)
      return
    }

    this.updateRevokedIssuers(revokedIssuersData)
  }

  private async getRevokedIssuersFromFile(): Promise<string> {
    return await fs.readFile(this.revocationListPath, 'utf8')
  }

  private async getRevokedIssuersFromUrl(): Promise<string> {
    const revokedIssuersReq = await firstValueFrom(this.httpService.get<string>(this.revocationListURL))
    if (revokedIssuersReq.status !== 200) {
      throw 'revoked issuers request response code not 200, unable to retrieve revoked issuers'
    }
    return revokedIssuersReq.data
  }

  private updateRevokedIssuers(revokedIssuersData: string) {
    this._revokedIssuers.clear()
    revokedIssuersData.split('\n\n').forEach(fileEntry => {
      if (!fileEntry) return
      const issuerName = fileEntry.substring(0, fileEntry.indexOf('\n'))
      const cert = new X509()
      cert.readCertPEM(fileEntry.replace(`${issuerName}\n`, ''))
      this._revokedIssuers.set(issuerName, cert)
    })
    this.logger.log('Revocation list parsed')
  }

  areCertificatesNotRevoked(issuerCerts: X509[]): boolean {
    let result = true
    issuerCerts.forEach(cert => {
      this._revokedIssuers.forEach((revokedCert, issuer) => {
        if (this.doesCertificatesMatch(cert, revokedCert)) {
          this.logger.warn(`certificate of ${issuer} is revoked thus no longer valid`)
          result = false
        }
      })
    })
    return result
  }

  private doesCertificatesMatch(cert: X509, revokedCert: X509) {
    return cert.getIssuerString() === revokedCert.getIssuerString() && cert.getSerialNumberHex() === revokedCert.getSerialNumberHex()
  }

  get revokedIssuers() {
    // Cloning the map to avoid modification
    return new Map(this._revokedIssuers)
  }
}
