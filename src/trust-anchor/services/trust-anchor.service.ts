import { HttpService } from '@nestjs/axios'
import { BadRequestException, ConflictException, Injectable, Logger, NotFoundException } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import * as crypto from 'crypto'
import { promises as fs } from 'fs'
import { ValidationResult } from 'joi'
import { KEYUTIL, X509, hextob64 } from 'jsrsasign'
import { Model } from 'mongoose'
import { TrustStates } from '../../common/interfaces'
import { isEmpty } from '../../common/util'
import { DEFAULT_UPSERT_OPTIONS } from '../constants'
import { CreateTrustAnchorDto, TrustAnchorRequestDto, TrustAnchorResponseDto } from '../dto'
import { TrustAnchor, TrustAnchorDocument } from '../schemas'
import { IssuersRevocationListService } from './issuers-revocation-list.service'

const retries = 3

@Injectable()
export class TrustAnchorService {
  private readonly logger = new Logger(TrustAnchorService.name)
  private trustAnchorCache = new Set<string>() // Cache to store hashed certificates

  constructor(
    @InjectModel(TrustAnchor.name) public trustAnchorModel: Model<TrustAnchorDocument>,
    private readonly issuersRevocationListService: IssuersRevocationListService,
    protected readonly httpService: HttpService
  ) {}

  /**
   * Validates a chain of X509 certificates
   * @param base64Certs the raw base64 encoded certificate strings
   * @returns {boolean} the verification result
   */
  public async validateCertChain(base64Certs: string[]): Promise<boolean> {
    const jsrsasignCerts = base64Certs.map(cert => this.base64ToX509(cert))

    await this.verifyCertificateChain(jsrsasignCerts)
    await this.verifyRootCertificate(jsrsasignCerts)

    if (!this.issuersRevocationListService.areCertificatesNotRevoked(jsrsasignCerts)) {
      throw new ConflictException('Issuer is in revocation list')
    }

    return true // the chain is valid
  }

  private async verifyCertificateChain(jsrsasignCerts: X509[]) {
    for (let i = 0; i < jsrsasignCerts.length - 1; i++) {
      const childCert = jsrsasignCerts[i] // Child certificate
      const issuerCert = jsrsasignCerts[i + 1] // Issuer's certificate

      if (!this.verifySignature(childCert, issuerCert)) {
        throw new ConflictException(`Certificate ${childCert.getSerialNumberHex()} issued by ${childCert.getIssuer().str} invalid signature.`)
      }
    }

    return true // the chain is valid
  }

  /**
   * Verifies that a child certificate is signed by its issuer.
   * @param {X509} childCert Child certificate in X509 format.
   * @param {X509} issuerCert Issuer certificate in X509 format.
   * @returns {boolean} Returns true if the signature is valid, false otherwise.
   */
  private verifySignature(childCert: X509, issuerCert: X509): boolean {
    try {
      // Extract public key from issuer certificate
      const issuerPublicKey = KEYUTIL.getKey(issuerCert.getPublicKey())
      // verify that child certificate is signed by issuer
      return childCert.verifySignature(issuerPublicKey)
    } catch (error) {
      this.logger.error('Signature verification failed', error)
      return false
    }
  }

  private async verifyRootCertificate(jsrsasignCerts: X509[]) {
    const rootCert = jsrsasignCerts[jsrsasignCerts.length - 1] // Root certificate

    try {
      // Verify the root certificate is self-signed
      const rootPublicKey = KEYUTIL.getKey(rootCert.getPublicKey())
      const isSelfSigned = rootCert.verifySignature(rootPublicKey)

      if (!isSelfSigned) {
        throw new ConflictException('Root certificate is not self-signed.')
      }

      // Verify the root certificate is in the trusted CAStore
      if (!(await this.isCertificateTrusted(rootCert))) {
        throw new NotFoundException('Root certificate not trusted.')
      }
    } catch (error) {
      throw new ConflictException('Unable to validate cert chain root certificate: ' + error.message)
    }
    return true
  }

  private async isCertificateTrusted(certToCheck: X509): Promise<boolean> {
    const b64Cert = hextob64(certToCheck.hex)
    const hashedCert = this.hashCertificate(b64Cert)
    return this.trustAnchorCache.has(hashedCert)
  }

  /**
   * Searches for a trustAnchor in the database given a certain certificate and returns a trustAnchorResponse if one was found.
   * Will throw a 404 exception in case no trustAnchor could be retrieved from the database.
   * @param trustAnchorData the body containing a certificate
   * @returns {Promise<TrustAnchorResponseDto>} the prepared trustAnchorResponse
   */
  public async findTrustAnchor(trustAnchorData: ValidationResult<TrustAnchorRequestDto>['value']): Promise<TrustAnchorResponseDto> {
    if (isEmpty(trustAnchorData)) throw new BadRequestException('Request body invalid.')
    const { certificate } = trustAnchorData
    const findTrustAnchor = await this.trustAnchorModel.findOne({ certificate }).exec()

    if (!findTrustAnchor) throw new NotFoundException('Trust Anchor not found.')

    return await this.prepareTrustAnchorResponse(findTrustAnchor)
  }

  /**
   * Prepares a trustAnchorResponse given a certain trustAnchor
   * @param trustAnchor the trustAnchor to prepare the response object for
   * @returns {Promise<TrustAnchorResponseDto>} the prepared trustAnchorResponse
   */
  private async prepareTrustAnchorResponse(trustAnchor: TrustAnchorDocument): Promise<TrustAnchorResponseDto> {
    return {
      trustState: TrustStates.Trusted,
      trustedForAttributes: new RegExp('.*', 'gm').toString(),
      trustedAt: trustAnchor.lastTimeOfTrust?.getTime()
    }
  }

  /**
   * Load trust anchors hashes from the db into memory
   *
   */
  public async loadTrustAnchorsCache(): Promise<void> {
    try {
      const anchors = await this.trustAnchorModel.find().exec()
      this.trustAnchorCache.clear() // Clear existing cache before reloading
      anchors.forEach(anchor => {
        const hashedCert = this.hashCertificate(anchor.certificate)
        this.trustAnchorCache.add(hashedCert)
      })
      this.logger.log('Trust anchor cache has been populated.')
    } catch (error) {
      this.logger.error('Failed to load trust anchor cache', error)
    }
  }

  /**
   * Update TrustAnchors in the DB from a given {@link CreateTrustAnchorDto} array
   * @param {CreateTrustAnchorDto[]} trustAnchors the trust anchors to update
   * @param {Object} options an options object
   * @param {QueryOptions} options query options to be passed to the database update call
   * @returns {Promise<number>} a promise resolving to the number of db entries that were updated
   */
  async updateTrustAnchors(trustAnchors: CreateTrustAnchorDto[], options: object = DEFAULT_UPSERT_OPTIONS): Promise<number> {
    const bulkWrites = []
    // Delete everything except manually pushed trust anchors
    bulkWrites.push({
      deleteMany: {
        filter: {
          name: {
            $not: {
              $regex: '^keep_.*'
            }
          }
        }
      }
    })
    for (const ta of trustAnchors) {
      const { certificate, _list } = ta

      bulkWrites.push({
        updateOne: {
          filter: { certificate, _list },
          update: {
            $set: { ...ta }
          },
          ...options
        }
      })
    }
    return await this.bulkInsertWithRetries(bulkWrites)
  }

  async bulkInsertWithRetries(bulkWrites: any[], retriesCount: number = 0) {
    try {
      const bulkWriteResult = await this.trustAnchorModel.bulkWrite(bulkWrites, { maxTimeMS: 10_000 })
      this.logger.log(`deleted ${bulkWriteResult.deletedCount} entries, created ${bulkWriteResult.upsertedCount}`)
      return bulkWriteResult.ok
    } catch (mongoException) {
      this.logger.error('Mongo error occurred when updating TrustedAnchors', mongoException)
      if (retriesCount < retries) {
        await this.bulkInsertWithRetries(bulkWrites, retriesCount++)
      } else {
        throw mongoException
      }
    }
  }

  async insertAISBLNotaryCert() {
    try {
      const notaryCertPath = '/data/ipfs/registry/x509CertificateChain.pem'
      let notaryCert = await fs.readFile(notaryCertPath, 'utf8')

      notaryCert = notaryCert.replace('-----BEGIN CERTIFICATE-----', '')
      notaryCert = notaryCert.replace('-----END CERTIFICATE-----', '')
      notaryCert = notaryCert.replace(/\r?\n|\r/gm, '')
      notaryCert = notaryCert.trim()

      await this.trustAnchorModel.create({
        name: 'Gaia-X AISBL Notary',
        certificate: notaryCert,
        _list: []
      })
    } catch (e) {
      this.logger.error('Unable to load the AISBL notary certificate', e)
    }
  }

  public getTrustAnchors(): Promise<TrustAnchor[]> {
    return this.trustAnchorModel.find().exec()
  }

  private base64ToX509(b64Certificate: string): X509 {
    const pem = `-----BEGIN CERTIFICATE-----\n${b64Certificate}\n-----END CERTIFICATE-----`
    const x509 = new X509()
    x509.readCertPEM(pem)
    return x509
  }

  private hashCertificate(cert: string): string {
    const hash = crypto.createHash('sha256')
    hash.update(cert)
    return hash.digest('hex')
  }
}
