import { HttpModule, HttpService } from '@nestjs/axios'
import { Test, TestingModule } from '@nestjs/testing'
import { AxiosResponse } from 'axios'
import { X509 } from 'jsrsasign'
import { of } from 'rxjs'
import { IssuersRevocationListService } from './issuers-revocation-list.service'
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require('fs')

jest.mock('fs', () => ({
  promises: {
    readFile: jest.fn()
  }
}))

const config = { url: 'http://localhost:3000/mockUrl', headers: null }

function createTestX509(certPem) {
  const cert = new X509()
  cert.readCertPEM(certPem)
  return cert
}

describe('IssuersRevocationListService', () => {
  let issuersRevocationListService: IssuersRevocationListService
  let httpService: HttpService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [IssuersRevocationListService]
    }).compile()

    issuersRevocationListService = module.get<IssuersRevocationListService>(IssuersRevocationListService)
    httpService = module.get<HttpService>(HttpService)
  })

  describe('fetchRevokedIssuers - revocation list querying and parsing', () => {
    async function initRevocationList() {
      const response: AxiosResponse<any> = {
        data: revocationListSeveral,
        headers: {},
        config,
        status: 200,
        statusText: 'OK'
      }
      jest.spyOn(httpService, 'get').mockImplementationOnce(() => of(response))
      const data = revocationListSeveral
      fs.promises.readFile.mockResolvedValue(data)
      await issuersRevocationListService.loadRevokedIssuers()
      expect(issuersRevocationListService.revokedIssuers).toBeDefined()
      expect(issuersRevocationListService.revokedIssuers.size).toEqual(2)
    }

    it('loads and parses an empty revocation list', async () => {
      const data = revocationListEmpty

      fs.promises.readFile.mockResolvedValue(data)
      await issuersRevocationListService.loadRevokedIssuers()
      expect(issuersRevocationListService.revokedIssuers).toBeDefined()
      expect(issuersRevocationListService.revokedIssuers.size).toEqual(0)
    })

    it('single entry revocation list', async () => {
      const data = revocationListUnitary
      fs.promises.readFile.mockResolvedValue(data)

      await issuersRevocationListService.loadRevokedIssuers()
      expect(issuersRevocationListService.revokedIssuers).toBeDefined()
      expect(issuersRevocationListService.revokedIssuers.size).toEqual(1)
    })
    it('multiple entries revocation list', async () => {
      const data = revocationListSeveral

      fs.promises.readFile.mockResolvedValue(data)

      await issuersRevocationListService.loadRevokedIssuers()
      expect(issuersRevocationListService.revokedIssuers).toBeDefined()
      expect(issuersRevocationListService.revokedIssuers.size).toEqual(2)
    })

    it('should not empty list if error occurs during file read', async () => {
      const data = revocationListSeveral
      fs.promises.readFile.mockResolvedValue(data)
      await issuersRevocationListService.loadRevokedIssuers()

      fs.promises.readFile.mockRejectedValue(new Error('File read error'))
      await issuersRevocationListService.loadRevokedIssuers()
      expect(issuersRevocationListService.revokedIssuers).toBeDefined()
      expect(issuersRevocationListService.revokedIssuers.size).toEqual(2)
    })
    it('should not empty list if not http 200', async () => {
      process.env.revocationListURL = 'http://mockedurl.com'
      await initRevocationList()
      const erroneousResponse: AxiosResponse<any> = {
        data: '',
        headers: {},
        config,
        status: 400,
        statusText: 'Bad Request'
      }
      jest.spyOn(httpService, 'get').mockImplementationOnce(() => of(erroneousResponse))
      await issuersRevocationListService.loadRevokedIssuers()
      expect(issuersRevocationListService.revokedIssuers).toBeDefined()
      expect(issuersRevocationListService.revokedIssuers.size).toEqual(2)
    })

    it('should not empty list if error occurs during query', async () => {
      process.env.revocationListURL = 'http://mockedurl.com'
      await initRevocationList()

      jest.spyOn(httpService, 'get').mockImplementationOnce(() => {
        throw Error()
      })
      await issuersRevocationListService.loadRevokedIssuers()
      expect(issuersRevocationListService.revokedIssuers).toBeDefined()
      expect(issuersRevocationListService.revokedIssuers.size).toEqual(2)
    })
  })

  describe('isIssuerRevoked - verify absence/presence of certs in revocation list', () => {
    beforeEach(async () => {
      //init revocation list with several entries
      const data = revocationListSeveral

      fs.promises.readFile.mockResolvedValue(data)
      await issuersRevocationListService.loadRevokedIssuers()
    })
    it('should return true when cert is not revoked', async () => {
      const nonRevokedCert = createTestX509(nonRevokedCertPem)

      const isIssuerRevoked = issuersRevocationListService.areCertificatesNotRevoked([nonRevokedCert])
      expect(isIssuerRevoked).toBeTruthy()
    })
    it('should return false when cert is revoked', async () => {
      const revokedCert = createTestX509(revokedCertPem)

      const isIssuerRevoked = issuersRevocationListService.areCertificatesNotRevoked([revokedCert])
      expect(isIssuerRevoked).toBeFalsy()
    })
    it('should return false when a cert in the list is revoked', async () => {
      const nonRevokedCert = createTestX509(nonRevokedCertPem)
      const revokedCert = createTestX509(revokedCertPem)

      const isIssuerRevoked = issuersRevocationListService.areCertificatesNotRevoked([nonRevokedCert, revokedCert])
      expect(isIssuerRevoked).toBeFalsy()
    })
  })
})
const nonRevokedCertPem = `-----BEGIN CERTIFICATE-----
MIIDdzCCAl+gAwIBAgIEAgAAuTANBgkqhkiG9w0BAQUFADBaMQswCQYDVQQGEwJJ
RTESMBAGA1UEChMJQmFsdGltb3JlMRMwEQYDVQQLEwpDeWJlclRydXN0MSIwIAYD
VQQDExlCYWx0aW1vcmUgQ3liZXJUcnVzdCBSb290MB4XDTAwMDUxMjE4NDYwMFoX
DTI1MDUxMjIzNTkwMFowWjELMAkGA1UEBhMCSUUxEjAQBgNVBAoTCUJhbHRpbW9y
ZTETMBEGA1UECxMKQ3liZXJUcnVzdDEiMCAGA1UEAxMZQmFsdGltb3JlIEN5YmVy
VHJ1c3QgUm9vdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKMEuyKr
mD1X6CZymrV51Cni4eiVgLGw41uOKymaZN+hXe2wCQVt2yguzmKiYv60iNoS6zjr
IZ3AQSsBUnuId9Mcj8e6uYi1agnnc+gRQKfRzMpijS3ljwumUNKoUMMo6vWrJYeK
mpYcqWe4PwzV9/lSEy/CG9VwcPCPwBLKBsua4dnKM3p31vjsufFoREJIE9LAwqSu
XmD+tqYF/LTdB1kC1FkYmGP1pWPgkAx9XbIGevOF6uvUA65ehD5f/xXtabz5OTZy
dc93Uk3zyZAsuT3lySNTPx8kmCFcB5kpvcY67Oduhjprl3RjM71oGDHweI12v/ye
jl0qhqdNkNwnGjkCAwEAAaNFMEMwHQYDVR0OBBYEFOWdWTCCR1jMrPoIVDaGezq1
BE3wMBIGA1UdEwEB/wQIMAYBAf8CAQMwDgYDVR0PAQH/BAQDAgEGMA0GCSqGSIb3
DQEBBQUAA4IBAQCFDF2O5G9RaEIFoN27TyclhAO992T9Ldcw46QQF+vaKSm2eT92
9hkTI7gQCvlYpNRhcL0EYWoSihfVCr3FvDB81ukMJY2GQE/szKN+OMY3EU/t3Wgx
jkzSswF07r51XgdIGn9w/xZchMB5hbgF/X++ZRGjD8ACtPhSNzkE1akxehi/oCr0
Epn3o0WC4zxe9Z2etciefC7IpJ5OCBRLbf1wbWsaY71k5h+3zvDyny67G7fyUIhz
ksLi4xaNmjICq44Y3ekQEe5+NauQrz4wlHrQMz2nZQ/1/I6eYs9HRCwBXbsdtTLS
R9I4LtD+gdwyah617jzV/OeBHRnDJELqYzmp
-----END CERTIFICATE-----`

const revokedCertPem = `-----BEGIN CERTIFICATE-----
MIIDlTCCAn2gAwIBAgIUEGFTkCD14InkxAyvlo1oMssIisUwDQYJKoZIhvcNAQEL
BQAwWjELMAkGA1UEBhMCRlIxDTALBgNVBAgMBE5vcmQxDjAMBgNVBAcMBUxpbGxl
MRgwFgYDVQQKDA9HYWlhLVggTGFiIExpc3QxEjAQBgNVBAMMCWxvY2FsaG9zdDAe
Fw0yMzAzMDcxMDM2MTRaFw0yNDAzMDYxMDM2MTRaMFoxCzAJBgNVBAYTAkZSMQ0w
CwYDVQQIDAROb3JkMQ4wDAYDVQQHDAVMaWxsZTEYMBYGA1UECgwPR2FpYS1YIExh
YiBMaXN0MRIwEAYDVQQDDAlsb2NhbGhvc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IB
DwAwggEKAoIBAQCjdKCZCvkzKNZxFQJgY+nQ/l1QMnD1U9EIavDB0KJFA97vM9/j
WHAQ/5SG8kzWUcuvofm5Aic3HKVeaihE+NsnrTGRKs0YtyMLJDGxmPcmisfcWNtR
iYuhkEofxZSfm7eUWmfJIPh3a2jPzhqJZk86TOalpTa19TmpQH3L5SuntVWX8YMI
Oe5t86TMKlAR7+UKzStFdPXI55Wqeh/TSkDLBurrGvhIVWWfbiW5uUV6JStyUfi7
W0urQRWgqsHtYI++LsVtsR7QgjW5TB8QNH3QnrSHow6yoHKKM6YJJJYtNDJdf5SZ
n9P7Zovrjo8nZIo6FLz86ZWMt5yfSPhf4us7AgMBAAGjUzBRMB0GA1UdDgQWBBQP
ceXNTNqIeFWBeu0twsIvvcSZ0DAfBgNVHSMEGDAWgBQPceXNTNqIeFWBeu0twsIv
vcSZ0DAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAbrD7DPMvp
OVNDyIEcyjVKSEeq0o3zqm7PKnhzmn2G/NmPkrIS7UEfuBaK9v7rOsIHCnxOqDf1
tNGVBi/xaAuns5DvPZZK2ezXm6wDXhXYYI6O9FMH9zWWnsw/M28Cx05Wkw4SjxUn
+/ezHgQk1GHT9BhrdEswYZhJzAiyupGbz5cB5OJ+HzXuGRXbcnctMAaUujehjAgp
/S8Yld5ttL4XeG4D7UM3+FRSDJbolyPDEmUHQ1U7lq4648FU+JV37+0Hcpjj9FDa
8nW/O6sV6PUVBgk/ZYfAjQ5ievzrdoSG0K8I64MlLZCMKOHmET4+KbDccRjhA/3+
NrdDQ/P1aPlM
-----END CERTIFICATE-----`

const revocationListEmpty = ``

const revocationListUnitary = `test issuer revocation
-----BEGIN CERTIFICATE-----
MIIDlTCCAn2gAwIBAgIUEGFTkCD14InkxAyvlo1oMssIisUwDQYJKoZIhvcNAQEL
BQAwWjELMAkGA1UEBhMCRlIxDTALBgNVBAgMBE5vcmQxDjAMBgNVBAcMBUxpbGxl
MRgwFgYDVQQKDA9HYWlhLVggTGFiIExpc3QxEjAQBgNVBAMMCWxvY2FsaG9zdDAe
Fw0yMzAzMDcxMDM2MTRaFw0yNDAzMDYxMDM2MTRaMFoxCzAJBgNVBAYTAkZSMQ0w
CwYDVQQIDAROb3JkMQ4wDAYDVQQHDAVMaWxsZTEYMBYGA1UECgwPR2FpYS1YIExh
YiBMaXN0MRIwEAYDVQQDDAlsb2NhbGhvc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IB
DwAwggEKAoIBAQCjdKCZCvkzKNZxFQJgY+nQ/l1QMnD1U9EIavDB0KJFA97vM9/j
WHAQ/5SG8kzWUcuvofm5Aic3HKVeaihE+NsnrTGRKs0YtyMLJDGxmPcmisfcWNtR
iYuhkEofxZSfm7eUWmfJIPh3a2jPzhqJZk86TOalpTa19TmpQH3L5SuntVWX8YMI
Oe5t86TMKlAR7+UKzStFdPXI55Wqeh/TSkDLBurrGvhIVWWfbiW5uUV6JStyUfi7
W0urQRWgqsHtYI++LsVtsR7QgjW5TB8QNH3QnrSHow6yoHKKM6YJJJYtNDJdf5SZ
n9P7Zovrjo8nZIo6FLz86ZWMt5yfSPhf4us7AgMBAAGjUzBRMB0GA1UdDgQWBBQP
ceXNTNqIeFWBeu0twsIvvcSZ0DAfBgNVHSMEGDAWgBQPceXNTNqIeFWBeu0twsIv
vcSZ0DAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAbrD7DPMvp
OVNDyIEcyjVKSEeq0o3zqm7PKnhzmn2G/NmPkrIS7UEfuBaK9v7rOsIHCnxOqDf1
tNGVBi/xaAuns5DvPZZK2ezXm6wDXhXYYI6O9FMH9zWWnsw/M28Cx05Wkw4SjxUn
+/ezHgQk1GHT9BhrdEswYZhJzAiyupGbz5cB5OJ+HzXuGRXbcnctMAaUujehjAgp
/S8Yld5ttL4XeG4D7UM3+FRSDJbolyPDEmUHQ1U7lq4648FU+JV37+0Hcpjj9FDa
8nW/O6sV6PUVBgk/ZYfAjQ5ievzrdoSG0K8I64MlLZCMKOHmET4+KbDccRjhA/3+
NrdDQ/P1aPlM
-----END CERTIFICATE-----
`

const revocationListSeveral = `test issuer revocation
-----BEGIN CERTIFICATE-----
MIIDlTCCAn2gAwIBAgIUEGFTkCD14InkxAyvlo1oMssIisUwDQYJKoZIhvcNAQEL
BQAwWjELMAkGA1UEBhMCRlIxDTALBgNVBAgMBE5vcmQxDjAMBgNVBAcMBUxpbGxl
MRgwFgYDVQQKDA9HYWlhLVggTGFiIExpc3QxEjAQBgNVBAMMCWxvY2FsaG9zdDAe
Fw0yMzAzMDcxMDM2MTRaFw0yNDAzMDYxMDM2MTRaMFoxCzAJBgNVBAYTAkZSMQ0w
CwYDVQQIDAROb3JkMQ4wDAYDVQQHDAVMaWxsZTEYMBYGA1UECgwPR2FpYS1YIExh
YiBMaXN0MRIwEAYDVQQDDAlsb2NhbGhvc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IB
DwAwggEKAoIBAQCjdKCZCvkzKNZxFQJgY+nQ/l1QMnD1U9EIavDB0KJFA97vM9/j
WHAQ/5SG8kzWUcuvofm5Aic3HKVeaihE+NsnrTGRKs0YtyMLJDGxmPcmisfcWNtR
iYuhkEofxZSfm7eUWmfJIPh3a2jPzhqJZk86TOalpTa19TmpQH3L5SuntVWX8YMI
Oe5t86TMKlAR7+UKzStFdPXI55Wqeh/TSkDLBurrGvhIVWWfbiW5uUV6JStyUfi7
W0urQRWgqsHtYI++LsVtsR7QgjW5TB8QNH3QnrSHow6yoHKKM6YJJJYtNDJdf5SZ
n9P7Zovrjo8nZIo6FLz86ZWMt5yfSPhf4us7AgMBAAGjUzBRMB0GA1UdDgQWBBQP
ceXNTNqIeFWBeu0twsIvvcSZ0DAfBgNVHSMEGDAWgBQPceXNTNqIeFWBeu0twsIv
vcSZ0DAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAbrD7DPMvp
OVNDyIEcyjVKSEeq0o3zqm7PKnhzmn2G/NmPkrIS7UEfuBaK9v7rOsIHCnxOqDf1
tNGVBi/xaAuns5DvPZZK2ezXm6wDXhXYYI6O9FMH9zWWnsw/M28Cx05Wkw4SjxUn
+/ezHgQk1GHT9BhrdEswYZhJzAiyupGbz5cB5OJ+HzXuGRXbcnctMAaUujehjAgp
/S8Yld5ttL4XeG4D7UM3+FRSDJbolyPDEmUHQ1U7lq4648FU+JV37+0Hcpjj9FDa
8nW/O6sV6PUVBgk/ZYfAjQ5ievzrdoSG0K8I64MlLZCMKOHmET4+KbDccRjhA/3+
NrdDQ/P1aPlM
-----END CERTIFICATE-----


second issuer revocation
-----BEGIN CERTIFICATE-----
MIIDlTCCAn2gAwIBAgIUEGFTkCD14InkxAyvlo1oMssIisUwDQYJKoZIhvcNAQEL
BQAwWjELMAkGA1UEBhMCRlIxDTALBgNVBAgMBE5vcmQxDjAMBgNVBAcMBUxpbGxl
MRgwFgYDVQQKDA9HYWlhLVggTGFiIExpc3QxEjAQBgNVBAMMCWxvY2FsaG9zdDAe
Fw0yMzAzMDcxMDM2MTRaFw0yNDAzMDYxMDM2MTRaMFoxCzAJBgNVBAYTAkZSMQ0w
CwYDVQQIDAROb3JkMQ4wDAYDVQQHDAVMaWxsZTEYMBYGA1UECgwPR2FpYS1YIExh
YiBMaXN0MRIwEAYDVQQDDAlsb2NhbGhvc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IB
DwAwggEKAoIBAQCjdKCZCvkzKNZxFQJgY+nQ/l1QMnD1U9EIavDB0KJFA97vM9/j
WHAQ/5SG8kzWUcuvofm5Aic3HKVeaihE+NsnrTGRKs0YtyMLJDGxmPcmisfcWNtR
iYuhkEofxZSfm7eUWmfJIPh3a2jPzhqJZk86TOalpTa19TmpQH3L5SuntVWX8YMI
Oe5t86TMKlAR7+UKzStFdPXI55Wqeh/TSkDLBurrGvhIVWWfbiW5uUV6JStyUfi7
W0urQRWgqsHtYI++LsVtsR7QgjW5TB8QNH3QnrSHow6yoHKKM6YJJJYtNDJdf5SZ
n9P7Zovrjo8nZIo6FLz86ZWMt5yfSPhf4us7AgMBAAGjUzBRMB0GA1UdDgQWBBQP
ceXNTNqIeFWBeu0twsIvvcSZ0DAfBgNVHSMEGDAWgBQPceXNTNqIeFWBeu0twsIv
vcSZ0DAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAbrD7DPMvp
OVNDyIEcyjVKSEeq0o3zqm7PKnhzmn2G/NmPkrIS7UEfuBaK9v7rOsIHCnxOqDf1
tNGVBi/xaAuns5DvPZZK2ezXm6wDXhXYYI6O9FMH9zWWnsw/M28Cx05Wkw4SjxUn
+/ezHgQk1GHT9BhrdEswYZhJzAiyupGbz5cB5OJ+HzXuGRXbcnctMAaUujehjAgp
/S8Yld5ttL4XeG4D7UM3+FRSDJbolyPDEmUHQ1U7lq4648FU+JV37+0Hcpjj9FDa
8nW/O6sV6PUVBgk/ZYfAjQ5ievzrdoSG0K8I64MlLZCMKOHmET4+KbDccRjhA/3+
NrdDQ/P1aPlM
-----END CERTIFICATE-----`
