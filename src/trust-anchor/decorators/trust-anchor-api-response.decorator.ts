import { applyDecorators, HttpCode, HttpStatus } from '@nestjs/common'
import { ApiBadRequestResponse, ApiBody, ApiOkResponse, ApiOperation, ApiProperty } from '@nestjs/swagger'
import { ExamplesObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface'

class TrustAnchorApiResponseType {
  @ApiProperty()
  result: boolean
}
export function TrustAnchorApiResponse(summary: string, dto: any, examples?: ExamplesObject) {
  return applyDecorators(
    HttpCode(HttpStatus.OK),
    ApiOperation({ summary }),
    ApiBody({ type: dto, examples, description: summary }),
    ApiBadRequestResponse({ description: 'Invalid request payload' }),
    ApiOkResponse({
      description: `TrustAnchor or root for chain was found in the registry`,
      type: TrustAnchorApiResponseType
    })
  )
}
