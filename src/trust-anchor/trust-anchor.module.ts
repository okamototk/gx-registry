import { HttpModule } from '@nestjs/axios'
import { CacheModule } from '@nestjs/cache-manager'
import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { ScheduleModule } from '@nestjs/schedule'
import { CertChainTransformPipe } from './pipes'
import { TrustAnchor, TrustAnchorList, TrustAnchorListSchema, TrustAnchorSchema } from './schemas'
import { EiDASTrustedListParserService, MozillaCAListParserService, TrustAnchorListService, TrustAnchorService } from './services'
import { IssuersRevocationListService } from './services/issuers-revocation-list.service'
import { TrustAnchorController } from './trust-anchor.controller'

@Module({
  imports: [
    HttpModule,
    ScheduleModule.forRoot(),
    CacheModule.register(),
    MongooseModule.forFeature([{ name: TrustAnchor.name, schema: TrustAnchorSchema }]),
    MongooseModule.forFeature([{ name: TrustAnchorList.name, schema: TrustAnchorListSchema }])
  ],
  controllers: [TrustAnchorController],
  providers: [
    EiDASTrustedListParserService,
    IssuersRevocationListService,
    MozillaCAListParserService,
    TrustAnchorService,
    TrustAnchorListService,
    CertChainTransformPipe
  ],
  exports: [TrustAnchorService, TrustAnchorListService]
})
export class TrustAnchorModule {}
