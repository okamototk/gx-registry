import { BadRequestException, Injectable, Logger, PipeTransform } from '@nestjs/common'
import { isNotEmpty } from '../../common/util'
import { splitPEM, stripPEMInfo } from '../../common/util/stripPem'
import { CertificateChainDto, TrustAnchorChainRequestDto } from '../dto'

@Injectable()
export class CertChainTransformPipe implements PipeTransform<TrustAnchorChainRequestDto | string, Promise<CertificateChainDto>> {
  private readonly logger = new Logger(CertChainTransformPipe.name)

  async transform(body: TrustAnchorChainRequestDto | string): Promise<CertificateChainDto> {
    try {
      let certs: string[]

      if (typeof body === 'object') {
        certs = Array.isArray(body.certs) ? body.certs : splitPEM(body.certs)
        // remove BEGIN CERTIFICATE etc. and filter empty leftover strings
        certs = certs.map(stripPEMInfo).filter(isNotEmpty)
      } else {
        certs = splitPEM(body)
      }

      return { certs }
    } catch (error) {
      this.logger.error(error)
      throw new BadRequestException('Certificate chain could not be transformed.')
    }
  }
}
