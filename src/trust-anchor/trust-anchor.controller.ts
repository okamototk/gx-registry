import { Body, ConflictException, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common'
import { ApiConflictResponse, ApiConsumes, ApiNotFoundResponse, ApiTags } from '@nestjs/swagger'
import { certificateChainRequest, certificateChainUriRequest, trustAnchorV2Request } from '../tests/fixtures/certificates.json'
import { TrustAnchorApiResponse } from './decorators'
import { CertsBody } from './decorators/certs-request.decorator'
import { CertificateChainDto, TrustAnchorChainUriRequestDto, TrustAnchorRequestDto } from './dto'
import { CertChainUriTransformPipe } from './pipes'
import { TrustAnchorService } from './services'

@ApiTags('TrustAnchor')
@Controller({ path: '/api/trustAnchor' })
export class TrustAnchorController {
  constructor(private readonly trustAnchorService: TrustAnchorService) {}

  @Post()
  @TrustAnchorApiResponse('Search for a TrustAnchor certificate in the registry', TrustAnchorRequestDto, {
    trustAnchor: { summary: 'Example Certificate', value: trustAnchorV2Request }
  })
  @HttpCode(HttpStatus.OK)
  @ApiNotFoundResponse({ description: `TrustAnchor was not be found in the registry` })
  async findTrustAnchor(@Body() trustAnchorRequestDto: TrustAnchorRequestDto) {
    return this.trustAnchorService.findTrustAnchor(trustAnchorRequestDto)
  }

  @Post('chain')
  @TrustAnchorApiResponse('Verify root of a certificate chain to be a TrustAnchor in the registry', String, {
    certChain: { summary: 'Example Certificate Chain in JSON', value: certificateChainRequest },
    certChainRaw: { summary: 'Example raw Certificate Chain', value: certificateChainRequest.certs }
  })
  @ApiConsumes('application/json', 'text/plain', 'application/x-pem-file', 'application/x-x509-ca-cert', 'x-x509-user-cert')
  @ApiConflictResponse({ description: `Root for the certificate chain could not be verified as a TrustAnchor in the registry` })
  async verifyTrustAnchorChainRaw(@CertsBody() certificateChainRaw: CertificateChainDto): Promise<{ result: true }> {
    return this.validateCertificateChain(certificateChainRaw.certs)
  }

  @Post('chain/file')
  @TrustAnchorApiResponse(
    'Verify root of a certificate chain, provided as a file at uri, to be a TrustAnchor in the registry',
    TrustAnchorChainUriRequestDto,
    {
      certChain: { summary: 'Example Certificate Chain', value: certificateChainUriRequest }
    }
  )
  @ApiConflictResponse({ description: `Root for the certificate chain could not be verified as a TrustAnchor in the registry` })
  async verifyTrustAnchorChain(@Body(CertChainUriTransformPipe) certificateChainRaw: CertificateChainDto): Promise<{ result: true }> {
    return this.validateCertificateChain(certificateChainRaw.certs)
  }

  async validateCertificateChain(certificates: string[]): Promise<{ result: true }> {
    let result: boolean
    try {
      result = await this.trustAnchorService.validateCertChain(certificates)
    } catch (Error) {
      throw new ConflictException('Unable to validate certificate chain', Error.message)
    }

    if (!result) throw new ConflictException(result)

    return { result }
  }
}
