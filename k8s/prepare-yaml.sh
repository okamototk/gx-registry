#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Usage: $0 <deploymentUrl> <version>"
    exit 1
fi

deploymentUrl=$1
version=$2

# Check if environment variables exist
if [ -z "$registryKey" ] || [ -z "$registryCert" ]; then
    echo "Error: Environment variables registryKey or registryCert are not set."
    exit 1
fi

# (Optional) check if environment variables for XAdES trust anchor list signing exist
if [ -z "$xadesPrivateKey" ] || [ -z "$xadesCertificate" ]; then
    echo "Warning: Environment variables xadesPrivateKey or xadesCertificate are not set."
fi

# Create a YAML file and handle multiline values
cat <<EOF >custom-values.yaml
nameOverride: $version
image:
  tag: $version
ingress:
  hosts:
   - host: $deploymentUrl
     paths:
       - path: /$version
         pathType: Prefix
privateKey: |
$(echo "$registryKey" | sed 's/^/  /')
x509Certificate: |
$(echo "$registryCert" | sed 's/^/  /')
xadesPrivateKey: |
$(echo "$xadesPrivateKey" | sed 's/^/  /')
xadesCertificate: |
$(echo "$xadesCertificate" | sed 's/^/  /')
EOF


if [ ! -z "$ISEVSSLONLY" ]; then
    echo "evsslonly: $ISEVSSLONLY" >> custom-values.yaml
else
    echo "evsslonly: true" >> custom-values.yaml
fi