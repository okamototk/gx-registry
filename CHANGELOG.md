# [2.4.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v2.3.0...v2.4.0) (2024-05-21)


### Features

* Add EC support during trust anchor verification ([f3a5527](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/f3a5527e96fa393b970449c6061b85d050ee9379))
* **LAB-617:** expose a LinkML ontology endpoint ([29c7fe8](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/29c7fe819eef7d796b61643956f1a15e65fe08c6))
* remove ipfs record prefix expectation in dns txt records ([c10191a](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/c10191aa927f4d36a5a48a3d289015cf43ce8acc))

# [2.3.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v2.2.0...v2.3.0) (2024-05-13)


### Bug Fixes

* **LAB-603:** fix optional version shape API parameter ([e0784b6](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/e0784b6d7b2c025bf09d6e99d8f906bab1cb7d68))


### Features

* **LAB-600:** remove deprecated /shapes/implemented endpoint ([4b061e5](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/4b061e5ff8b81684b27286d591685c89e60b25e9))
* **LAB-601:** modify the shapes endpoint ([da9f4ac](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/da9f4acf0eb7049f3072465365a2e9fbe4d05c23))
* **LAB-601:** modify the shapes endpoint ([919a566](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/919a5666b54e833a1ad1742034f7405b8c45162d))
* **LAB-602:** remove deprecated /api/trusted-schemas-registry/v2/schemas/:key endpoint ([4d3b85c](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/4d3b85c1567a0f06a2131bf88adea015f9d06699))
* **LAB-603:** update schemas API to /schemas/:version ([98621e5](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/98621e5751a95e06b8956cb45988997e6f5cb703))
* **LAB-605:** implement /owl/:version? endpoint to expose OWL ontologies ([cb6fb35](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/cb6fb35c2b3ba717c22c917ef64005a0c4de79e6))

# [2.2.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v2.1.0...v2.2.0) (2024-04-16)


### Bug Fixes

* **LAB-466:** prevent error when _list is not set in trust anchor ([6b06c20](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/6b06c20abf42b3f2d926fee492038cab5369fada))
* strip ipfs uri before parsing cid ([5e28989](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/5e289890e5df7affd3ac11b0cd111956805b3fe2))


### Features

* Auto configure kubo with ingress defined values ([ee20382](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/ee2038286407e42a61f7d476bbe98eff30e91e6c))
* fetch ipfs artefacts for current deployment branch ([db4489f](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/db4489f970e229634cbfefb30d78f78bc5e3a69b))
* **LAB-466:** expose trusted anchors using ETSI TS 119 612 format ([7af30c8](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/7af30c8f5d3c611ca5665ac65ce7dc9541f96251))

# [2.1.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v2.0.0...v2.1.0) (2024-02-28)


### Bug Fixes

* connection timeout at startup ([c889d38](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/c889d387d3bb8c708e2445e974461d5f4d060967))
* **LAB-529:** handle errors on anchors retrieval ([d2b538e](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/d2b538e8fefa95adcbf31c6a9b154933f8a8f4a0))
* missing keypair secret ([aa0d752](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/aa0d752e708dc0097fb6055b6c0980fdc06d6f38))
* missing keypair secret ([354a092](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/354a09243b6a56ad5bea8f1e8a7d1c00701d4254))
* secret format for key and cert ([7ac8554](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/7ac85544dff310aa743f5d9761f9f7b2d97bf318))
* service offering shape resource aggregation ([ec5101a](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/ec5101ab05ce5cf9085a6a6896ae71c71dc659e7))


### Features

* add kyverno policy ([3c17f65](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/3c17f656cbf5fbca8a8fd016aeb2320c33c70583))
* allow override of crucial configurations ([2323454](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/2323454490ab80c13b975ab4071a50160d066cdf))
* **LAB-407:** allow raw x509 payload for trust anchor verification endpoint ([e94ab36](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/e94ab36822d08d9a1918342ca9576f43439c489f))
* remove deprecated complianceIssuers module ([f204a4d](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/f204a4d1316356d4f1bd0c6f16b427b1cee19098))
* use an ipfs client to load registry artefacts ([4535967](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/45359671aac66887f73486b049cf407dd97dffdc))

# [2.0.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.9.0...v2.0.0) (2023-11-22)


### chore

* branch-off tagus loire ([35d96eb](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/35d96eb00de06e600cf48169b6e07870f7227566))


### BREAKING CHANGES

* switch to v2

Signed-off-by: Ewann Gavard <ewann.gavard@gaia-x.eu>

# [1.9.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.8.0...v1.9.0) (2023-11-21)


### Bug Fixes

* fix invalid criterion name ([228d252](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/228d252b543f470fe3b6713860dd8e32cb85ebd1))


### Features

* add label shapes ([92ac423](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/92ac4231afdbd89f161c18cae7948f7761561d0b))

# [1.8.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.7.1...v1.8.0) (2023-10-23)


### Bug Fixes

* retrieve AISBL notary cert at startup ([c8a8164](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/c8a81648e9af5a7789da3574c903717e9e1543c4)), closes [gaia-x/lab/gxdch#8](https://gitlab.com/gaia-x/lab/gxdch/issues/8)


### Features

* add missing shapes to trust framework shacl validator ([e4666c8](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/e4666c82b70b3a2571538d958af39cf522590502))

## [1.7.1](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.7.0...v1.7.1) (2023-09-12)


### Bug Fixes

* service offering dependsOn shape invalid ([1df1c41](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/1df1c41891e692fd0938354cc8703d9a4fce50bf))

# [1.7.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.6.1...v1.7.0) (2023-08-28)


### Bug Fixes

* all certificates are revoked ([c1798e2](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/c1798e2b5d2abe4288dc84b988c865c9dcd1bcf2))
* **TAG-181:** Fixup deployment BASE_URL ([3dfe7ab](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/3dfe7abecfed40b40696fe77413626ffd15fe3af))
* **trust-anchors:** make evsslonly the default behavior ([8abac59](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/8abac598294526d9b5ad2876a0fbc8edda5aa899))


### Features

* **TAG-112:** validate issuer's terms and conditions ([1458d8b](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/1458d8b14ccbf896578259f586552decd20bb111))
* **trust-anchors:** manage a revocation list ([c8cc58f](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/c8cc58f2ed36953e9a6db028f45841c02b21018e))

## [1.6.1](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.6.0...v1.6.1) (2023-06-21)


### Bug Fixes

* publish changelogs on slack ([1c9067d](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/1c9067d3bef41d6e5027d3fe0e328e99be3f662e))

# [1.6.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.5.0...v1.6.0) (2023-06-21)


### Features

* publish changelogs on slack ([cf1863b](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/cf1863b0b69a18f3ba3234e16e5a367604d277bb))

# [1.5.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.4.5...v1.5.0) (2023-06-20)


### Features

* list trusted issuers for each dch service ([06803b0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/06803b018b856dbc4167df1df1612c8ec8ff9b13))

## [1.4.5](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.4.4...v1.4.5) (2023-06-08)


### Bug Fixes

* json schema should contain a context field ([383860b](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/383860b03ca346114c1b35e0cddb4bc1111353d3))

## [1.4.4](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.4.3...v1.4.4) (2023-06-07)


### Bug Fixes

* return a proper schema for Participant empty for other ([6bf7460](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/6bf74600797f572982213e42c41a6ffea078e3eb))

## [1.4.3](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.4.2...v1.4.3) (2023-06-06)


### Bug Fixes

* make schema return the proper context based on request ([7514417](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/7514417a6f66823c11d4d4940b855efaadec2e5f))

## [1.4.2](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.4.1...v1.4.2) (2023-06-06)


### Bug Fixes

* make schema registry return ok http code ([f397464](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/f397464d25e87313b17d55e2e233ec4c68720ec1))

## [1.4.1](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.4.0...v1.4.1) (2023-05-24)


### Bug Fixes

* code cleanup ([63822b9](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/63822b97435d862c03ae97fc1457cb863ed6a011))

# [1.4.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.3.1...v1.4.0) (2023-05-23)


### Bug Fixes

* prevent deleting keep_ certificates from TA ([59507cd](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/59507cd62a0a5139f9d8d0eb61f0d67068eea73b))


### Features

* add evsslonly flag to limit valid certs to EV only ([caf2d47](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/caf2d479766a90dd22ae55301a82b473d98b4fad))
* propose versionning of shapes ([58e2494](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/58e249441094970d585d91fed82a21c8e0897af6))
* re-enable force update flag for trust anchor ([002ad65](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/002ad6514a310f729c1a59edf4a3a90e1e51a4dc))

## [1.3.1](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.3.0...v1.3.1) (2023-05-09)


### Bug Fixes

* typo in trustframework.ttl ([dbeb314](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/dbeb314777c7138387765db922f0bdb6333d4676))

# [1.3.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.2.0...v1.3.0) (2023-04-27)


### Features

* expose current registry url ([aa9f63d](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/aa9f63d86a7e079252153325b4f3f913ea68157c))

# [1.2.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.1.1...v1.2.0) (2023-04-24)


### Features

* expose implemented trust framework shapes ([d2d9e2d](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/d2d9e2dea994952acd8381bad649e05878a93772))

## [1.1.1](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.1.0...v1.1.1) (2023-04-18)


### Bug Fixes

* tag proper docker images ([1727119](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/17271194640aa436cab9b156e19e2d5911300eb2))

# [1.1.0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/compare/v1.0.0...v1.1.0) (2023-04-13)


### Bug Fixes

* avoid breaking change by aliasing participant etc ([939a238](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/939a238d02fdbc849b623cb5c26fa20795cc7808))


### Features

* enforce certificates trust ([069a205](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/069a2052a7fcb4cd2d6bd1689f97a1a7517aba50))

# 1.0.0 (2023-03-28)


### Bug Fixes

* add 2 seconds timeout on anchors fetch ([96cce95](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/96cce95deb9078e217e6542c5abf90b2500fbbe8))
* add missing env file for mongodb ([cb0bb62](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/cb0bb6282e6c28d8619afe7188f94282048f097c))
* cert chain not validated properly ([bf5eb7b](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/bf5eb7b9d70c5ab3be4818b5e63782b68d73bef3))
* fix docs route in index ([063a9ae](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/063a9aef7c09a871ff61fec1c7e2a67d8cf99374))
* fix path of static shapes ([a5b247e](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/a5b247e82e8bc32f624257bef828880b829a85d8))
* fix postinstall script for production env ([cfce8ef](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/cfce8ef8bc12229e2051f57f859773fb4a85dc59))
* fix regex patterns in service-offering ([e4df04a](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/e4df04a64c3d1080dec9de1ba935b33607e0620a))
* fix statuscode for /trustAnchor res to be 200 ([1f4fc77](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/1f4fc77a92122a5101ce551d980fb430db7e180a))
* fix typos in Dockerfile ([4244ab0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/4244ab0041eadeca4fac3e042f55cd6638befef8))
* fix unresolvable imports ([3ad5cd0](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/3ad5cd009c227e1c1a62e858fe3de99191882748))
* import paths ([e60f58f](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/e60f58f03a16a80a79067aaaa2b0ff5c2a663cfc))
* mongodb startup with docker-compose ([a008e06](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/a008e06527cea9599bcc587858c4ff98036a8565))
* pattern for base url in ttl ([4038f80](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/4038f80399ec970694a33a68b1df3ad312778e97))
* provide TF2210 jsonLD schemas ([d290120](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/d29012089a9521e055c9831358612648492c411d))
* remove null from isolated nodes ([05dfbb5](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/05dfbb5e68868889ff2936534416a29ff5e92962))
* remove second typescript import ([bbb6c29](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/bbb6c29bc5fc3ef92e5cbba7323449373268415a))
* tests ([f57aeda](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/f57aeda881e7aaec860da4fb7e1c99aa80d844f3))
* use correct function name for findAndUpdateOrCreateTal ([d3dda16](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/d3dda16f992ad183a7ddb01ea3969707603dd2ae))
* use env vars for mongodb connection ([f93249f](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/f93249fa19f4c0005496f1b5f8db578036f6ed2b))


### Features

* add creation/updating to csv route [WIP] ([1737e1e](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/1737e1ea9c16ff0604c2803555af041057cd735a))
* add csv parsing ([587c471](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/587c47175c51d03f7e99ece5f8ec79f6291157fb))
* add dumb-init to address nodejs PID 1 issues ([fa833cd](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/fa833cd19d9be05ae26cd27f8c2bd2fe013e043e))
* add example gitlab-ci ([300bbae](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/300bbae0afc9653add9af5db50d44b0f2da12fb3))
* add getObjPropertyByPath helper function ([f50a260](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/f50a2600766bd0a2f078e00388a389bca08dadb6))
* add mozilla csv list parser [WIP] ([c5585c4](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/c5585c4d5b17173ca6d87838f7518b3bc74a62e7))
* add MozillaCAListParser class ([4e869e7](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/4e869e7f4fc06c094bbab5ebf0d9cd5e8f532e26))
* add parsing for ec eiDAS lotl [WIP] ([28372ba](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/28372babe50ac7dee0fe7fa54feabdddfac2f8d8))
* add trustFramework.json and create trustAnchorList.service ([91c929b](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/91c929b28f029f9d0666ed99f06c95aaf153bacb))
* add unique index to anchor list schema ([febd4cc](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/febd4ccae8dc8bf8d3c3f4006a9e87de334a17f5))
* add version prefix to api and docs path ([b4cf697](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/b4cf69772bdc58191336124287344ba5fb2099f3))
* add version prefix to api and docs path ([83f7a90](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/83f7a9017066a7750afff9f900041f46edc6837b))
* add xml parser for trust anchor lists in xml format ([a62f88a](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/a62f88a2581fcaf5da260ed2d7fd428fcf9b8f28))
* change participant shape ([db6e0ed](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/db6e0edf2ce3e5f4fbc1ad59f2f13e9c946fb8c0))
* create abstract tal parser class & adjust tal model ([a1e5ede](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/a1e5edea09c77f04b27853367fba1575871f45fd))
* expose shapes as jsonld and use baseURL in prefix ([82c9afb](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/82c9afb996f9c2fffcc4e3bd066ddb2f067c6e18))
* jsonld endpoint returns ld+json content type ([b1e0841](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/b1e08416db4e9ab1655a7c99fe21ab6a769646d9))
* update participant shape ([0be2237](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/0be2237eec03fa3306d26c691342d3bed2910a9a))
* use env for setting the port ([5eeea01](https://gitlab.com/gaia-x/lab/compliance/gx-registry/commit/5eeea017c7a09abd455dfdc926456c8ccd807ef3))
