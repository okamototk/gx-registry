FROM node:20-buster as development-build-stage

WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y \
    build-essential \
    git \
    libssl-dev \
    openjdk-11-jdk \
    ant \
    && rm -rf /var/lib/apt/lists/*

ENV JAVA_HOME /usr/lib/jvm/default-java
RUN export JAVA_HOME

COPY package*.json ./
RUN npm install --only=development

COPY . .

RUN npm run build

# Production Stage
FROM node:20-buster as production-build-stage

ENV NODE_ENV=production

WORKDIR /usr/src/app

COPY --from=development-build-stage /usr/src/app/node_modules ./node_modules
COPY --from=development-build-stage /usr/src/app/dist ./dist

COPY package*.json ./

COPY . .

CMD ["node", "dist/src/main"]
